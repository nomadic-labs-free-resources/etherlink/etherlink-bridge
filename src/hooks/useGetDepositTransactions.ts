import { useAccount } from '@nlfr/use-tezos';
import { useIndexerTransfers } from '@/lib/use-outbox-indexer';

export const useGetDepositTransactions = ({ page }: { page: number }) => {
  const { address } = useAccount();

  const { transactions, isLoading, isError, hasNextPage } = useIndexerTransfers({
    query: {
      page,
      limit: 50,
      filters: { sender: String(address), type: 'DEPOSIT' },
      sort: [{ orderBy: 'tezosTimestamp', order: 'DESC' }],
    },
  });

  return { transactions, isLoading, isError, hasNextPage };
};
