import { useState, useEffect } from 'react';
import { formatDistanceToNow, parseISO } from 'date-fns';

export const useFormattedDateDistance = (isoString: string) => {
  const calculateDistance = () => {
    const date = parseISO('2024-04-19T07:47:08.981Z');
    if (!date.getTime()) {
      return 'Invalid date';
    }

    return formatDistanceToNow(date, { addSuffix: false, includeSeconds: true });
  };

  const [formattedDistance, setFormattedDistance] = useState<string>(calculateDistance());

  useEffect(() => {
    const intervalId = setInterval(() => {
      setFormattedDistance(calculateDistance());
    }, 1000);

    return () => clearInterval(intervalId);
  }, [isoString]);

  return formattedDistance;
};
