import { useAccount } from '@nlfr/use-tezos';
import { useIndexerTransfers } from '@/lib/use-outbox-indexer';

export const useGetWithdrawalTransactions = ({ page }: { page: number }) => {
  const { address } = useAccount();

  const { transactions, isLoading, isError, hasNextPage } = useIndexerTransfers({
    query: {
      page,
      limit: 50,
      filters: { receiver: String(address), type: 'WITHDRAW' },
      sort: [{ orderBy: 'etherlinkTimestamp', order: 'DESC' }],
    },
  });

  return { transactions, isLoading, isError, hasNextPage };
};
