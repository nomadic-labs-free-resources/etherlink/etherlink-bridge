import { useEffect, useState } from 'react';
import { useAccount, useTezos } from '@nlfr/use-tezos';
import { mutezToTez } from '@/utils/utils';

export const useGetTezosBalance = () => {
  const [balance, setBalance] = useState<number | null>(null);
  const { address } = useAccount();
  const { toolkit } = useTezos();

  useEffect(() => {
    async function fetchBalance() {
      if (toolkit && address) {
        try {
          const fetchedBalance = await toolkit.tz.getBalance(address);
          // setBalance(fetchedBalance.toNumber() / 1000000);
          setBalance(mutezToTez(fetchedBalance.toNumber()));
        } catch (error) {
          // eslint-disable-next-line no-console
          console.error('Error fetching balance: ', error);
        }
      }
    }

    fetchBalance();
  }, [address]);

  return {
    address,
    balance,
  };
};
