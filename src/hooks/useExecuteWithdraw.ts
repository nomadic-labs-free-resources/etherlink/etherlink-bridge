import { useOutboxIndexerProof } from '@/lib/use-outbox-indexer';
import { NotificationSteps, showNotification } from '@/utils/notifications';

export const useExecuteWithdraw = () => {
  const { executeProof, isLoading } = useOutboxIndexerProof();

  const executeTransaction = async (level: string, index: string) => {
    try {
      await executeProof(Number(level), Number(index));

      showNotification(NotificationSteps.Success, 'Transaction executed successfully');
    } catch (error: any) {
      showNotification(
        NotificationSteps.Failure,
        `${error.response?.data?.message}: ${error.response?.data?.error}`
      );
    }
  };

  return { executeTransaction, isLoading };
};
