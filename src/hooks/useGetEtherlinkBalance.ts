import { useAccount, useBalance } from 'wagmi';

export const useGetEtherlinkBalance = () => {
  const { address } = useAccount();
  const balance = useBalance({
    address,
  });

  return {
    address,
    balance,
  };
};
