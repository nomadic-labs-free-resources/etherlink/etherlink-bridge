import { useEffect, useState } from 'react';
import { useTezos } from '@nlfr/use-tezos';

export const useGetHead = () => {
  const [head, setHead] = useState<number>(0);
  const { toolkit } = useTezos();

  useEffect(() => {
    async function fetchBalance() {
      if (toolkit) {
        try {
          const block = await toolkit.rpc.getBlockHeader();
          setHead(block.level);
        } catch (error) {
          // eslint-disable-next-line no-console
          console.error('Error fetching head: ', error);
        }
      }
    }

    fetchBalance();
  }, []);

  return head;
};
