import { useState, useEffect } from 'react';
import { addSeconds, intervalToDuration, formatDuration } from 'date-fns';
import { useGetHead } from './useGetHead';

export const useTransactionCementationTime = (messageLevel: number): string => {
  const [timeRemaining, setTimeRemaining] = useState<string>('Loading...');
  const headLevel = useGetHead();

  const getEndDate = (blocksRemaining: number, blockTime: number): Date => {
    const secondsToAdd = blocksRemaining * blockTime;
    return addSeconds(new Date(), secondsToAdd);
  };

  const updateRemainingTime = async () => {
    const blockTime = 10;
    const blocksToCement = 120960;
    const blocksRemaining = messageLevel + blocksToCement - headLevel;
    if (blocksRemaining > 0) {
      const endDate = getEndDate(blocksRemaining, blockTime);
      const now = new Date();
      if (endDate > now) {
        const duration = intervalToDuration({ start: now, end: endDate });
        const formattedTime = formatDuration(duration, {
          format: ['days', 'hours'],
          delimiter: ' ',
        });
        setTimeRemaining(`${formattedTime}`);
      } else {
        setTimeRemaining('Transaction finalized');
      }
    }
  };

  useEffect(() => {
    updateRemainingTime();
    const intervalId = setInterval(updateRemainingTime, 30000);
    return () => clearInterval(intervalId);
  }, [headLevel]);

  return timeRemaining;
};
