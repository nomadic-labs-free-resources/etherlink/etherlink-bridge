import { useEffect, useState } from 'react';
import { useDisclosure } from '@mantine/hooks';

import { Transaction } from '@/lib/use-outbox-indexer';

type UseTransferHistoryProps = {
  useTransfersHook: ({ page }: { page: number }) => {
    transactions: Transaction[];
    isLoading: boolean;
    isError: boolean;
    hasNextPage: boolean;
  };
  initialPage?: number;
};

export const useTransferHistory = ({
  useTransfersHook,
  initialPage = 1,
}: UseTransferHistoryProps) => {
  const [page, setPage] = useState(initialPage);
  const [transactions, setTransactions] = useState<Transaction[]>([]);
  const [modalOpened, modalControls] = useDisclosure(false);
  const [selectedTransaction, setSelectedTransaction] = useState<Transaction | null>(null);
  const {
    transactions: newTransactions,
    isLoading,
    isError,
    hasNextPage,
  } = useTransfersHook({ page: page || 1 });

  useEffect(() => {
    setTransactions((prev) => [...prev, ...newTransactions]);
  }, [newTransactions]);

  const openDetailsModal = (transaction: Transaction) => {
    setSelectedTransaction(transaction);
    modalControls.open();
  };

  return {
    transactions,
    isLoading,
    isError,
    hasNextPage,
    setPage,
    selectedTransaction,
    modalOpened,
    modalControls,
    openDetailsModal,
  };
};
