import useSWR from 'swr';

const fetcher = (url: string) => fetch(url).then((res) => res.json());

export function useFetchSwr(endpoint: string) {
  const apiBaseUrl = 'http://localhost:3000/';
  const url = `${apiBaseUrl}${endpoint}`;

  const { data: transactionsData, error } = useSWR(url, fetcher);

  return {
    transactionsData: transactionsData || [],
    hasNextPage: false,
    isLoading: !error && !transactionsData,
    isError: error,
  };
}
