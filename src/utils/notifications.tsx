import { NotificationData, notifications } from '@mantine/notifications';
import { IconAlertCircle, IconBroadcast, IconCheck, IconX } from '@tabler/icons-react';

export enum NotificationSteps {
  WalletActionRequired = 'WalletActionRequired',
  TransactionBroadcast = 'TransactionBroadcast',
  Success = 'Success',
  Failure = 'Failure',
}

const getNotificationConfig = (step: NotificationSteps, message: string): NotificationData => {
  switch (step) {
    case NotificationSteps.WalletActionRequired:
      return {
        title: 'Wallet Action Required',
        icon: <IconAlertCircle size="1.1rem" />,
        color: 'orange',
        autoClose: false,
        message,
      };
    case NotificationSteps.TransactionBroadcast:
      return {
        title: 'Transaction Broadcast',
        icon: <IconBroadcast size="1.1rem" />,
        color: 'blue',
        autoClose: false,
        message,
      };
    case NotificationSteps.Success:
      return {
        title: 'Success',
        icon: <IconCheck size="1.1rem" />,
        color: 'teal',
        autoClose: 10000,
        message,
      };
    case NotificationSteps.Failure:
      return {
        title: 'Error',
        icon: <IconX size="1.1rem" />,
        color: 'red',
        autoClose: 10000,
        message,
      };
    default:
      return {
        title: '',
        message,
      };
  }
};

// Function to show a notification and return its ID
export const showNotification = (step: NotificationSteps, message: string) => {
  const config = getNotificationConfig(step, message);
  return notifications.show(config);
};

// Function to update an existing notification by its ID
export const updateNotification = (id: string, step: NotificationSteps, message: string) => {
  const config = getNotificationConfig(step, message);
  notifications.update({ id, ...config });
};
