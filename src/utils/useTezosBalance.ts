import { useState, useEffect } from 'react';
import { TezosToolkit } from '@taquito/taquito';
import { env } from '@/env.mjs';

export const useTezosBalance = (from: string) => {
  const [balance, setBalance] = useState<number | string | null>(null);

  useEffect(() => {
    const TezosBalance = new TezosToolkit(env.NEXT_PUBLIC_TZ_RPC_URL);

    let isMounted = true;

    async function fetchBalance() {
      if (isMounted && from) {
        try {
          const fetchedBalance = await TezosBalance.tz.getBalance(from);
          setBalance(fetchedBalance.toNumber() / 1000000);
        } catch (error) {
          setBalance('Error fetching balance');
        }
      }
    }

    fetchBalance();
    return () => {
      isMounted = false;
    };
  }, [from]);

  return balance;
};
