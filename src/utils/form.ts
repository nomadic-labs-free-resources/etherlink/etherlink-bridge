import { env } from '@/env.mjs';

export const validateAmount = (amount: number): string | null => {
  const LIMIT = env.NEXT_PUBLIC_APP_BRIDGE_AMOUNT_LIMIT;

  if (LIMIT <= 0 || (LIMIT && amount > 0 && amount <= LIMIT)) {
    return null;
  }

  return `Invalid amount: must be greater than 0 and less or equal than ${LIMIT}`;
};
