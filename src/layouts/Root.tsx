'use client';

import { FC, PropsWithChildren } from 'react';
import { NetworkType } from '@airgap/beacon-types';
import { createWeb3Modal } from '@web3modal/wagmi/react';

import { env } from '@/env.mjs';
import { EtherlinkBridgeProvider, etherlinkConfig } from '@/lib/etherlink-bridge';
import { AppShell } from './AppShell/AppShell';

createWeb3Modal({
  wagmiConfig: etherlinkConfig,
  projectId: env.NEXT_PUBLIC_ETHERLINK_PROJECT_ID,
  enableAnalytics: true,
});

export const RootLayout: FC<PropsWithChildren> = ({ children }) => (
  <EtherlinkBridgeProvider
    tezosConfig={{
      config: {
        dappName: 'Etherlink Bridge',
        rpcUrl: env.NEXT_PUBLIC_TZ_RPC_URL,
        network: env.NEXT_PUBLIC_TZ_NETWORK as NetworkType,
        iconUrl: 'https://bridge.etherlink.com/assets/etherlink.svg',
      },
    }}
    wagmiConfig={etherlinkConfig}
  >
    <AppShell>{children}</AppShell>
  </EtherlinkBridgeProvider>
);
