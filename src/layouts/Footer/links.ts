import { IconBrandGitlab } from '@tabler/icons-react';

import { env } from '@/env.mjs';

export const linksRight = [
  {
    key: 'LINK_GITLAB',
    link: env.NEXT_PUBLIC_APP_GIT,
    icon: IconBrandGitlab,
  },
];

export const linksLeft = [
  {
    key: 'BAKERY_VERSION',
    link: env.NEXT_PUBLIC_APP_VERSION,
  },
];
