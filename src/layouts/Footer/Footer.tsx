import {
  Text,
  Container,
  ActionIcon,
  Group,
  rem,
  Avatar,
  Title,
  useMantineTheme,
  Image,
  useMantineColorScheme,
} from '@mantine/core';
import {
  IconBrandX,
  IconBrandDiscord,
  IconBrandTelegram,
  IconBrandGithub,
} from '@tabler/icons-react';
import { env } from '@/env.mjs';

import classes from './Footer.module.css';

export function Footer() {
  const { primaryColor } = useMantineTheme();
  const { colorScheme } = useMantineColorScheme();
  const iconColor = primaryColor;

  return (
    <footer className={classes.footer}>
      <Container className={classes.inner}>
        <div className={classes.logo}>
          <Group>
            <Avatar size="md" src="/assets/etherlink.svg" />

            <Title size="h2" className={classes.etherlinkTitle}>
              <Text inherit component="span" className={classes.etherlinkSpan}>
                Etherlink
              </Text>
            </Title>
          </Group>
        </div>
        <div className={classes.groups}>
          <div className={classes.wrapper}>
            <Text className={classes.title}>Powered By</Text>
            <Group gap="md" className={classes.social} wrap="nowrap">
              {colorScheme === 'dark' ? (
                <Image src="/assets/tezos_logo_white.svg" alt="Tezos Logo" width={50} height={50} />
              ) : (
                <Image src="/assets/tezos_logo_black.svg" alt="Tezos Logo" width={50} height={50} />
              )}
            </Group>
          </div>

          <div className={classes.link}>
            <Text className={classes.title}>Join the community</Text>
            <Group gap="md" className={classes.social} wrap="nowrap">
              <ActionIcon
                size="lg"
                c={iconColor}
                variant="transparent"
                component="a"
                target="_blank"
                href={env.NEXT_PUBLIC_LINK_ETHERLINK_X}
              >
                <IconBrandX style={{ width: rem(64), height: rem(64) }} stroke={1.5} />
              </ActionIcon>
              <ActionIcon
                size="lg"
                c={iconColor}
                variant="transparent"
                component="a"
                target="_blank"
                href={env.NEXT_PUBLIC_LINK_ETHERLINK_DISCORD}
              >
                <IconBrandDiscord style={{ width: rem(64), height: rem(64) }} stroke={1.5} />
              </ActionIcon>
              <ActionIcon
                size="lg"
                c={iconColor}
                variant="transparent"
                component="a"
                target="_blank"
                href={env.NEXT_PUBLIC_LINK_ETHERLINK_TELEGRAM}
              >
                <IconBrandTelegram style={{ width: rem(64), height: rem(64) }} stroke={1.5} />
              </ActionIcon>
              <ActionIcon
                size="lg"
                c={iconColor}
                variant="transparent"
                component="a"
                target="_blank"
                href={env.NEXT_PUBLIC_LINK_ETHERLINK_GITHUB}
              >
                <IconBrandGithub style={{ width: rem(64), height: rem(64) }} stroke={1.5} />
              </ActionIcon>
            </Group>
          </div>
        </div>
      </Container>

      <Container className={classes.afterFooter}>
        <Text c="dimmed" size="sm">
          {env.NEXT_PUBLIC_DISCLAIMER_COPYRIGHT}
        </Text>

        <Group className={classes.social} justify="flex-end">
          <Text c="dimmed" size="sm" fs="italic">
            v{env.NEXT_PUBLIC_APP_VERSION}
          </Text>
        </Group>
      </Container>
    </footer>
  );
}
