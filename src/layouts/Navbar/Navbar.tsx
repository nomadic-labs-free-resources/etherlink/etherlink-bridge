'use client';

import { Button, Container, Stack } from '@mantine/core';
import { IconExternalLink } from '@tabler/icons-react';
import { FC } from 'react';

import { env } from '@/env.mjs';
import { ButtonWallet, ButtonWalletEvm } from '@/components/ui';
import { ConfigMenu } from '@/components';
import { HEADER_LINKS } from '../Header/links';
import { Drawer } from '../Drawer/Drawer';

export const Navbar: FC = () => {
  const links = HEADER_LINKS.map((link) => (
    <Button
      key={link.key}
      component="a"
      href={link.link}
      target="_blank"
      variant="subtle"
      color="gray"
      rightSection={<IconExternalLink size={14} />}
      size="compact-sm"
    >
      {link.label}
    </Button>
  ));

  return (
    <Container>
      <Stack align="flex-start">
        <ButtonWallet walletType="tezos" shorterAddress fullWidth />
        <ButtonWalletEvm walletType="eth" shorterAddress fullWidth />
        <ConfigMenu menuTargetLabel />

        {links}
      </Stack>
      {env.NEXT_PUBLIC_APP_WITHDRAW_ENABLED && <Drawer />}
    </Container>
  );
};
