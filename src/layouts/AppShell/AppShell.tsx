'use client';

import { FC, PropsWithChildren } from 'react';
import { AppShell as MantineAppShell, Container } from '@mantine/core';
import { Notifications } from '@mantine/notifications';
import { useDisclosure } from '@mantine/hooks';

import { Header } from '../Header/Header';
import { Navbar } from '../Navbar/Navbar';
import { Footer } from '../Footer/Footer';

export const AppShell: FC<PropsWithChildren> = ({ children }) => {
  const [opened, { toggle }] = useDisclosure();

  return (
    <MantineAppShell
      header={{ height: 60 }}
      navbar={{ width: 300, breakpoint: 'md', collapsed: { desktop: true, mobile: !opened } }}
    >
      <MantineAppShell.Header>
        <Header burgerOpened={opened} onBurgerClick={toggle} />
      </MantineAppShell.Header>

      <MantineAppShell.Navbar py="md" px={4}>
        <Navbar />
      </MantineAppShell.Navbar>

      <MantineAppShell.Main>
        <Container>{children}</Container>
        <Footer />
      </MantineAppShell.Main>

      <Notifications />
    </MantineAppShell>
  );
};
