import { env } from '@/env.mjs';

export const HEADER_LINKS = [
  {
    key: 'EXPLORER',
    label: 'Explorer',
    link: env.NEXT_PUBLIC_ETHERLINK_EXPLORER_URL,
  },
];
