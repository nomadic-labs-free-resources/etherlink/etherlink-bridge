'use client';

import { FC } from 'react';
import { Anchor, Avatar, Badge, Box, Burger, Button, Container, Group } from '@mantine/core';
import { IconExternalLink } from '@tabler/icons-react';

import { env } from '@/env.mjs';
import { ButtonWallet, ButtonWalletEvm } from '@/components/ui';
import { AppTitle, ConfigMenu } from '@/components';
import { Drawer } from '../Drawer/Drawer';
import classes from './Header.module.css';
import { HEADER_LINKS } from './links';

type Props = {
  burgerOpened: boolean;
  onBurgerClick: () => void;
};

export type HeaderProps = Props;
export const Header: FC<Props> = ({ burgerOpened, onBurgerClick }) => {
  const links = HEADER_LINKS.map((link) => (
    <Button
      key={link.key}
      component="a"
      href={link.link}
      target="_blank"
      variant="subtle"
      color="gray"
      rightSection={<IconExternalLink size={14} />}
      size="compact-sm"
      visibleFrom="md"
    >
      {link.label}
    </Button>
  ));

  return (
    <header className={classes.header}>
      <Container size="lg" className={classes.inner}>
        <Group justify="center">
          <Anchor href="/" underline="never">
            <Group gap={2}>
              <Avatar size="md" src="/assets/etherlink.svg" />
              <AppTitle />
            </Group>
          </Anchor>

          {env.NEXT_PUBLIC_TZ_NETWORK !== 'mainnet' && (
            <Badge variant="outline" color="grey">
              {env.NEXT_PUBLIC_TZ_NETWORK}
            </Badge>
          )}

          {links}

          {env.NEXT_PUBLIC_APP_WITHDRAW_ENABLED && (
            <Box visibleFrom="md">
              <Drawer />
            </Box>
          )}
        </Group>

        <Group visibleFrom="md">
          <ButtonWalletEvm walletType="eth" shorterAddress />
          <ButtonWallet walletType="tezos" shorterAddress />
          <ConfigMenu />
        </Group>

        <Burger opened={burgerOpened} onClick={onBurgerClick} hiddenFrom="md" size="sm" />
      </Container>
    </header>
  );
};
