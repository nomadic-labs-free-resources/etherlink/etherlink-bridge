import React, { FC } from 'react';
import { Box, Button, Container } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { IconHistory } from '@tabler/icons-react';
import { useAccount as useTezosAccount } from '@nlfr/use-tezos';

import { useGetWithdrawalTransactions } from '@/hooks';
import { Drawer as CustomDrawer, Tabs } from '@/components/ui';
import { ETransfer } from '@/features/TransfersHistory/TransferDetails/TransferDetails';
import { TransfersHistory } from '@/features/TransfersHistory/TransfersHistory/TransfersHistory';
import { useGetDepositTransactions } from '@/hooks/useGetDepositTransactions';

export const Drawer: FC = () => {
  const [opened, { open, close }] = useDisclosure(false);
  const { address } = useTezosAccount();

  return (
    <Box>
      <CustomDrawer opened={opened} onClose={close} title="Transaction History">
        <Container size="lg">
          <Tabs
            tabs={[
              {
                label: 'Deposits',
                value: 'deposits',
                content: (
                  <TransfersHistory
                    transferType={ETransfer.DEPOSIT}
                    useTransfersHook={useGetDepositTransactions}
                    loadingMessage="Loading deposits..."
                    errorMessage="Failed to load deposits"
                  />
                ),
              },
              {
                label: 'Withdrawals',
                value: 'withdrawals',
                content: (
                  <TransfersHistory
                    transferType={ETransfer.WITHDRAW}
                    useTransfersHook={useGetWithdrawalTransactions}
                    loadingMessage="Loading withdrawals..."
                    errorMessage="Failed to load withdrawals"
                  />
                ),
              },
            ]}
          />
        </Container>
      </CustomDrawer>

      {address && (
        <Button
          onClick={open}
          variant="subtle"
          color="gray"
          rightSection={<IconHistory size={14} />}
          size="compact-sm"
          visibleFrom="sm"
        >
          Transaction History
        </Button>
      )}
    </Box>
  );
};
