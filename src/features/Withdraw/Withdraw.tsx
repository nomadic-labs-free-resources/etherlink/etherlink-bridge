import { useState } from 'react';
import { Button, Divider, Stack, Box, Center } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { useAccount } from 'wagmi';
import { useAccount as useGetTezosBalance } from '@nlfr/use-tezos';
import { IconArrowDown } from '@tabler/icons-react';

import { InteractiveIcon, NumberInput, Paper, SwitchM, AddressInput } from '@/components/ui';
import {
  FormFromWallet,
  FormToWallet,
  ConfirmModal,
  TransferConfirmAction,
  DepositTestnetAlert,
  EtherlinkWalletBalance,
} from '@/components';
import { useWithdrawForm } from './hooks/useWithdrawForm';
import { useWithdrawAction } from './hooks/useWithdrawAction';

type Props = {
  onTabChange: (title: string) => void;
};

export type WithdrawProps = Props;
export function Withdraw({ onTabChange }: WithdrawProps) {
  const { address: from } = useAccount();
  const { address: to } = useGetTezosBalance();
  const form = useWithdrawForm(to);
  const [checked, setChecked] = useState(false);
  const withdraw = useWithdrawAction({ form, isManual: checked });
  const [opened, { open, close }] = useDisclosure(false);

  const handleSubmit = async (event: MouseEvent) => {
    event.preventDefault();

    if (
      (!checked
        ? !form.validateField('tzAddress').hasError
        : !form.validateField('manualTzAddress').hasError) &&
      !form.validateField('amount').hasError
    ) {
      open();
    }
  };

  const buttonText =
    !from || from.length === 0
      ? 'Connect your wallet to withdraw funds'
      : (!checked ? !form.values.tzAddress : !form.values.manualTzAddress) || !form.values.amount
        ? 'Fill out the form to withdraw funds'
        : 'Move funds to Tezos';

  return (
    <Box>
      <DepositTestnetAlert />

      <Stack>
        <Paper>
          <Stack>
            <FormToWallet
              label="From"
              buttonContent="Connect Etherlink Wallet"
              walletType="eth"
              buttonProps={{ fullWidth: true }}
              onConnect={() => {}}
            />
            <Box>
              <EtherlinkWalletBalance />
              <NumberInput form={form} inputPropsKey="amount" currency="xtz" />
            </Box>
          </Stack>
        </Paper>

        <Center>
          <InteractiveIcon Icon={IconArrowDown} onClick={() => onTabChange('Deposit')} />
        </Center>
        {!checked ? (
          <Paper>
            <Stack>
              <FormFromWallet
                label="To"
                buttonContent="Connect Tezos Wallet"
                walletType="tezos"
                buttonProps={{ fullWidth: true }}
                onConnect={() => {}}
              />
            </Stack>
          </Paper>
        ) : (
          <AddressInput form={form} inputPropsKey="manualTzAddress" placeholder="tz..." />
        )}
        <SwitchM
          checked={checked}
          label="I'm transferring to an address other than my own"
          setChecked={setChecked}
        />
      </Stack>

      <Divider my="xl" />

      <Stack>
        <Button
          variant="light"
          size="lg"
          onClick={(e: any) => handleSubmit(e)}
          disabled={buttonText !== 'Move funds to Tezos'}
        >
          {buttonText}
        </Button>
      </Stack>

      <ConfirmModal
        isOpen={opened}
        onClose={close}
        onConfirm={withdraw}
        amount={form.values.amount}
        ethAddress={from || ''}
        tzAddress={checked ? form.values.manualTzAddress : form.values.tzAddress}
        actionType={TransferConfirmAction.Withdraw}
      />
    </Box>
  );
}
