import { UseFormReturnType } from '@mantine/form';

import { env } from '@/env.mjs';
import { useWithdraw } from '@/lib/etherlink-bridge';
import { showNotification, updateNotification, NotificationSteps } from '@/utils/notifications';
import { WithdrawFormValues } from './useWithdrawForm';

export type UseWithdrawActionProps = {
  form: UseFormReturnType<WithdrawFormValues>;
  isManual: boolean;
};
export type UseWithdrawActionReturn = () => void;

export type UseWithdrawAction = (form: UseWithdrawActionProps) => () => Promise<void>;

export const useWithdrawAction: UseWithdrawAction = ({ form, isManual }) => {
  const withdraw = useWithdraw();

  const handleWithdraw = async () => {
    const idNotification = showNotification(
      NotificationSteps.WalletActionRequired,
      'Please confirm the withdraw in your wallet.'
    );

    try {
      await withdraw({
        tzAddress: isManual
          ? (form.values.manualTzAddress as string)
          : (form.values.tzAddress as string),
        amount: form.values.amount,
        bridgeContractAddress: env.NEXT_PUBLIC_ETHERLINK_BRIDGE_0X,
      });

      updateNotification(idNotification, NotificationSteps.Success, 'Withdraw successful!');
    } catch (err: any) {
      updateNotification(
        idNotification,
        NotificationSteps.Failure,
        `Withdraw failed: ${err.message}`
      );
      throw err;
    }
  };

  return handleWithdraw;
};
