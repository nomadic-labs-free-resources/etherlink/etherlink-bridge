import { useEffect } from 'react';
import { useForm } from '@mantine/form';
import { validateKeyHash } from '@taquito/utils';
import { validateAmount } from '@/utils/form';

export type WithdrawFormValues = {
  tzAddress?: string;
  manualTzAddress?: string;
  amount: number;
};

export const useWithdrawForm = (address: string | undefined) => {
  const form = useForm<WithdrawFormValues>({
    initialValues: {
      tzAddress: '',
      manualTzAddress: '',
      amount: 0,
    },
    validate: {
      tzAddress: (val) => (validateKeyHash(val as string) === 3 ? null : 'Invalid Tezos address'),
      manualTzAddress: (val) =>
        validateKeyHash(val as string) === 3 ? null : 'Invalid Tezos address',
      amount: (val) => validateAmount(val),
    },
    validateInputOnChange: true,
  });

  useEffect(() => {
    form.setValues({ tzAddress: address || '' });
  }, [address]);

  return form;
};
