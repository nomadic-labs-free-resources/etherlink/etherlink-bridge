import { useState } from 'react';
import { Button, Divider, Stack, Box, Center } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import { IconArrowDown } from '@tabler/icons-react';
import { useAccount } from '@nlfr/use-tezos';
import { useAccount as wagmiUseAccount } from 'wagmi';

import { env } from '@/env.mjs';
import { InteractiveIcon, NumberInput, Paper, AddressInput, SwitchM } from '@/components/ui';
import {
  DepositTestnetAlert,
  FormFromWallet,
  FormToWallet,
  ConfirmModal,
  TransferConfirmAction,
  TezosWalletBalance,
} from '@/components';
import { useDepositForm } from './hooks/useDepositForm';
import { useDepositAction } from './hooks/useDepositAction';

type Props = {
  onTabChange: (title: string) => void;
};

export type DepositProps = Props;
export function Deposit({ onTabChange }: DepositProps) {
  const { address } = useAccount();
  const { address: to } = wagmiUseAccount();
  const form = useDepositForm(to as string);
  const [checked, setChecked] = useState(false);
  const handleDeposit = useDepositAction({ form, isManual: checked });
  const [opened, { open, close }] = useDisclosure(false);

  const handleSubmit = async (event: MouseEvent) => {
    event.preventDefault();
    if (
      (!checked
        ? !form.validateField('ethAddress').hasError
        : !form.validateField('manualAddress').hasError) &&
      !form.validateField('amount').hasError
    ) {
      open();
    }
  };

  const buttonText =
    !address || address.length === 0
      ? 'Connect your wallet to deposit funds'
      : (!checked ? !form.values.ethAddress : !form.values.manualAddress) || !form.values.amount
        ? 'Fill out the form to deposit funds'
        : 'Move funds to Etherlink';

  return (
    <Box>
      <DepositTestnetAlert />

      <Stack>
        <Paper>
          <Stack>
            <FormFromWallet
              label="From"
              buttonContent="Connect Tezos Wallet"
              walletType="tezos"
              buttonProps={{ fullWidth: true }}
              onConnect={() => {}}
            />
            <Box>
              <TezosWalletBalance />
              <NumberInput form={form} inputPropsKey="amount" currency="xtz" />
            </Box>
          </Stack>
        </Paper>

        <Center>
          <InteractiveIcon
            Icon={IconArrowDown}
            onClick={() => onTabChange('Withdraw')}
            disabled={!env.NEXT_PUBLIC_APP_WITHDRAW_ENABLED}
          />
        </Center>
        {!checked ? (
          <Paper>
            <FormToWallet
              label="To"
              buttonContent="Connect Etherlink Wallet"
              walletType="eth"
              buttonProps={{ fullWidth: true }}
              onConnect={() => {}}
            />
          </Paper>
        ) : (
          <AddressInput form={form} inputPropsKey="manualAddress" placeholder="0x..." />
        )}
        <SwitchM
          checked={checked}
          label="I'm transferring to an address other than my own"
          setChecked={setChecked}
        />
      </Stack>

      <Divider my="xl" />

      <Stack>
        <Button
          variant="light"
          size="lg"
          onClick={(e: any) => handleSubmit(e)}
          disabled={buttonText !== 'Move funds to Etherlink'}
        >
          {buttonText}
        </Button>
      </Stack>

      <ConfirmModal
        isOpen={opened}
        onClose={close}
        onConfirm={handleDeposit}
        amount={form.values.amount}
        ethAddress={
          checked ? (form.values.manualAddress as string) : (form.values.ethAddress as string)
        }
        tzAddress={address}
        actionType={TransferConfirmAction.Deposit}
      />
    </Box>
  );
}
