import { showNotification, updateNotification, NotificationSteps } from '@/utils/notifications';
import { useDeposit } from '@/lib/etherlink-bridge';

import { env } from '@/env.mjs';
import { UseFormReturnType } from '@mantine/form';
import { DepositFormValues } from './useDepositForm';

export type UseDepositActionProps = {
  form: UseFormReturnType<DepositFormValues>;
  isManual: boolean;
};
export type UseDepositActionReturn = () => void;

export type UseDepositAction = (form: UseDepositActionProps) => () => Promise<void>;

export const useDepositAction: UseDepositAction = ({ form, isManual }) => {
  const deposit = useDeposit();

  const handleDeposit = async () => {
    const idNotification = showNotification(
      NotificationSteps.WalletActionRequired,
      'Please confirm the deposit in your wallet.'
    );

    try {
      const operationDeposit = await deposit({
        evmAddress: isManual
          ? (form.values.manualAddress as string)
          : (form.values.ethAddress as string),
        amount: form.values.amount,
        bridgeContractAddress: env.NEXT_PUBLIC_TZ_BRIDGE_KT,
        rollupContractAddress: env.NEXT_PUBLIC_TZ_ROLLUP_SR,
      });
      updateNotification(
        idNotification,
        NotificationSteps.TransactionBroadcast,
        'Transaction is being broadcasted to the network.'
      );

      await operationDeposit.confirmation();
      updateNotification(idNotification, NotificationSteps.Success, 'Deposit successful!');
    } catch (err: any) {
      updateNotification(
        idNotification,
        NotificationSteps.Failure,
        `Deposit failed: ${err.message}`
      );
      throw err;
    }
  };

  return handleDeposit;
};
