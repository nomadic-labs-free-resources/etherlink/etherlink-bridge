import { useForm } from '@mantine/form';
import { isAddress as isEthAddress } from 'web3-validator';
import { useAccountEffect } from 'wagmi';
import { validateAmount } from '@/utils/form';

export type DepositFormValues = {
  ethAddress?: string;
  manualAddress?: string;
  amount: number;
};

export const useDepositForm = (to: string) => {
  const form = useForm<DepositFormValues>({
    initialValues: {
      ethAddress: to,
      manualAddress: '',
      amount: 0,
    },
    validate: {
      ethAddress: (val) => (isEthAddress(val as string) ? null : 'Invalid Etherlink address'),
      manualAddress: (val) => (isEthAddress(val as string) ? null : 'Invalid Etherlink address'),
      amount: (val) => validateAmount(val),
    },
    validateInputOnChange: true,
  });

  useAccountEffect({
    onConnect(data) {
      form.setValues({ ethAddress: data.address });
    },
    onDisconnect() {
      form.setValues({ ethAddress: '' });
    },
  });

  return form;
};
