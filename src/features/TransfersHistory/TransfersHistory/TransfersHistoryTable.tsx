import React from 'react';
import { Button, Center, Table, Text } from '@mantine/core';

import { Transaction } from '@/lib/use-outbox-indexer';
import { formatDateAndTime } from '@/utils/withdraw';
import { mutezToTez } from '@/utils/utils';
import { NumberFormatter, Badge } from '@/components/ui';

type Props = {
  transactions: Transaction[];
  onTransactionClick: (transaction: Transaction) => void;
};

export type TransfersHistoryTableProps = Props;
export const TransfersHistoryTable: React.FC<TransfersHistoryTableProps> = ({
  transactions,
  onTransactionClick,
}) => {
  if (!transactions || transactions.length === 0) {
    return (
      <Center my="xl">
        <Text fz="xs" c="dimmed" fw="bold">
          No transfers found.
        </Text>
      </Center>
    );
  }

  return (
    <Table withRowBorders={false}>
      <Table.Thead>
        <Table.Tr>
          <Table.Th>Date</Table.Th>
          <Table.Th>Amount</Table.Th>
          <Table.Th>Status</Table.Th>
          <Table.Th />
        </Table.Tr>
      </Table.Thead>
      <Table.Tbody>
        {transactions.map((tx) => (
          <Table.Tr key={tx.id}>
            <Table.Td>
              <Text fz="xs" c="dimmed" fw="bold">
                {formatDateAndTime(tx.tezosTimestamp || tx.etherlinkTimestamp)}
              </Text>
            </Table.Td>
            <Table.Td>
              <NumberFormatter
                value={mutezToTez(Number(tx.amount))}
                currency="xtz"
                size="sm"
                weight={600}
              />
            </Table.Td>
            <Table.Td>
              <Badge status={tx.status} />
            </Table.Td>
            <Table.Td align="right">
              <Button size="xs" onClick={() => onTransactionClick(tx)} style={{ marginTop: '3px' }}>
                See Details
              </Button>
            </Table.Td>
          </Table.Tr>
        ))}
      </Table.Tbody>
    </Table>
  );
};
