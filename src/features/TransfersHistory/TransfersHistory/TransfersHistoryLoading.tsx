import { FC } from 'react';
import { Center, Stack, Loader, Text } from '@mantine/core';

type Props = {
  message: string;
  color: string;
};

export type TransfersHistoryLoadingProps = Props;
export const TransfersHistoryLoading: FC<TransfersHistoryLoadingProps> = ({ message, color }) => (
  <Center my="xl">
    <Stack align="center">
      <Loader color={color} type="bars" />
      <Text size="xs" c="dimmed" w="bold">
        {message}
      </Text>
    </Stack>
  </Center>
);
