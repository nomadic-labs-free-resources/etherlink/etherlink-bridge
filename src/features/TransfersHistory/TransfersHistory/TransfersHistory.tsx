import React, { FC } from 'react';
import { Button, Center, useMantineTheme } from '@mantine/core';

import { Transaction } from '@/lib/use-outbox-indexer';
import { useTransferHistory } from '@/hooks';
import { mutezToTez } from '@/utils/utils';
import { Badge, Modal } from '@/components/ui';
import { ETransfer, TransferDetails } from '../TransferDetails/TransferDetails';
import { TransfersHistoryTable } from './TransfersHistoryTable';
import { TransfersHistoryError } from './TransfersHistoryError';
import { TransfersHistoryLoading } from './TransfersHistoryLoading';

type Props = {
  transferType: ETransfer;
  useTransfersHook: ({ page }: { page: number }) => {
    transactions: Transaction[];
    isLoading: boolean;
    isError: boolean;
    hasNextPage: boolean;
  };
  loadingMessage: string;
  errorMessage: string;
};

export type TransfersHistoryProps = Props;
export const TransfersHistory: FC<TransfersHistoryProps> = ({
  transferType,
  useTransfersHook,
  loadingMessage,
  errorMessage,
}) => {
  const { colors } = useMantineTheme();
  const {
    transactions,
    isLoading,
    isError,
    hasNextPage,
    setPage,
    modalOpened,
    modalControls,
    selectedTransaction,
    openDetailsModal,
  } = useTransferHistory({ useTransfersHook, initialPage: 1 });

  if (isError) {
    return <TransfersHistoryError message={errorMessage} />;
  }
  if (!transactions.length && isLoading) {
    return <TransfersHistoryLoading message={loadingMessage} color={colors.etherlink[3]} />;
  }

  return (
    <>
      <TransfersHistoryTable transactions={transactions} onTransactionClick={openDetailsModal} />

      {isLoading && (
        <TransfersHistoryLoading message={loadingMessage} color={colors.etherlink[3]} />
      )}

      {transactions.length > 0 && hasNextPage && (
        <Center mt="md">
          <Button
            fullWidth
            onClick={() => setPage((prev) => prev + 1)}
            loading={isLoading}
            variant="light"
          >
            Load More
          </Button>
        </Center>
      )}
      {selectedTransaction && (
        <Modal
          isOpen={modalOpened}
          onClose={modalControls.close}
          title={{
            title: `${transferType} Details`,
            date: new Date(
              selectedTransaction.tezosTimestamp || selectedTransaction.etherlinkTimestamp
            ).toDateString(),
            status: <Badge status={selectedTransaction.status} />,
          }}
        >
          <TransferDetails
            type={transferType}
            transaction={{
              ...selectedTransaction,
              amount: String(mutezToTez(Number(selectedTransaction.amount))),
            }}
          />
        </Modal>
      )}
    </>
  );
};
