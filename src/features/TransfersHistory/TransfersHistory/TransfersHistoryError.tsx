import { FC } from 'react';
import { Center, Text } from '@mantine/core';

type Props = {
  message: string;
};

export type TransfersHistoryErrorProps = Props;
export const TransfersHistoryError: FC<TransfersHistoryErrorProps> = ({ message }) => (
  <Center my="xl">
    <Text size="xs" c="red" fw="bold">
      {message}
    </Text>
  </Center>
);
