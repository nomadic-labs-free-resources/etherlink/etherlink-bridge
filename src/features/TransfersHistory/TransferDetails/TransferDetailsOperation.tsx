import React, { FC } from 'react';
import { Table } from '@mantine/core';

import { TextAddress } from '@/components/ui';

type Props = {
  operation: string;
  network: 'tezos' | 'etherlink';
};

export type TransferDetailsBalanceUpdateProps = Props;
export const TransferDetailsOperation: FC<TransferDetailsBalanceUpdateProps> = ({
  operation,
  network,
}) => (
  <Table.ScrollContainer minWidth={500}>
    <Table withRowBorders={false}>
      <Table.Thead>
        <Table.Tr>
          <Table.Th>Operation</Table.Th>
        </Table.Tr>
      </Table.Thead>
      <Table.Tbody>
        <Table.Tr>
          <Table.Td>
            <TextAddress address={operation} network={network} tx />
          </Table.Td>
        </Table.Tr>
      </Table.Tbody>
    </Table>
  </Table.ScrollContainer>
);
