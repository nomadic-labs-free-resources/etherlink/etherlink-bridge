import React, { FC } from 'react';
import { Center, Divider, useMantineTheme, Box, Button } from '@mantine/core';
import { IconArrowDown } from '@tabler/icons-react';

import { Transaction, TransferStatus } from '@/lib/use-outbox-indexer';
import { useExecuteWithdraw } from '@/hooks';
import { FieldsetNetwork } from '@/components/ui';
import { WithdrawAlert } from '@/components';
import { TransferDetailsBalanceUpdate } from './TransferDetailsBalanceUpdate';
import { TransferDetailsStep } from './TransferDetailsStep';
import { TransferDetailsOperation } from './TransferDetailsOperation';

export enum ETransfer {
  DEPOSIT = 'Deposit',
  WITHDRAW = 'Withdraw',
}

type Props = {
  type: ETransfer;
  transaction: Transaction;
};

export const TransferDetails: FC<Props> = ({ type, transaction }) => {
  const { colors } = useMantineTheme();
  const { executeTransaction, isLoading } = useExecuteWithdraw();

  const network: Record<string, 'tezos' | 'etherlink'> = {
    from: type === ETransfer.DEPOSIT ? 'tezos' : 'etherlink',
    to: type === ETransfer.DEPOSIT ? 'etherlink' : 'tezos',
  };
  const fromOperation =
    type === ETransfer.DEPOSIT
      ? transaction.tezosOperationHash
      : transaction.etherlinkOperationHash;
  const toOperation =
    type === ETransfer.DEPOSIT
      ? transaction.etherlinkOperationHash
      : transaction.tezosOperationHash;

  return (
    <Box>
      {type === ETransfer.WITHDRAW && <WithdrawAlert />}

      <FieldsetNetwork network={network.from}>
        <TransferDetailsOperation operation={fromOperation} network={network.from} />
        <TransferDetailsBalanceUpdate
          amount={Number(transaction.amount)}
          network={network.from}
          address={transaction.sender}
          update="decrease"
        />
      </FieldsetNetwork>

      <Center mt="md" mb="xs">
        <IconArrowDown size="1.2rem" color={colors.etherlink[6]} />
      </Center>

      <FieldsetNetwork network={network.to}>
        {toOperation && <TransferDetailsOperation operation={toOperation} network={network.to} />}
        <TransferDetailsBalanceUpdate
          address={transaction.receiver}
          amount={Number(transaction.amount)}
          network={network.to}
          update="increase"
        />
      </FieldsetNetwork>

      <Divider my="lg" />

      {type === ETransfer.WITHDRAW ? (
        <>
          <TransferDetailsStep transaction={transaction} />

          <Button
            color={colors.etherlink[4]}
            size="lg"
            onClick={() => executeTransaction(transaction.message.level, transaction.message.level)}
            disabled={transaction.status !== TransferStatus.READY}
            fullWidth
            loading={isLoading}
          >
            Execute
          </Button>
        </>
      ) : null}
    </Box>
  );
};
