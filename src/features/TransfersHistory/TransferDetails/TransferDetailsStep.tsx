import React, { FC } from 'react';
import { IconArrowBigDownLines } from '@tabler/icons-react';

import { Transaction, TransferStatus } from '@/lib/use-outbox-indexer';
// import { useFormattedDateDistance } from '@/hooks';
import { Stepper } from '@/components/ui';
import { useTransactionCementationTime } from '@/hooks';

const getStepByStatus = (status: TransferStatus) => {
  switch (status) {
    case TransferStatus.CONFIRMED:
      return 3;
    case TransferStatus.READY:
      return 2;
    case TransferStatus.PENDING:
      return 1;
    default:
      return 0;
  }
};

type Props = {
  transaction: Transaction;
};

export type TransferDetailsProps = Props;
// eslint-disable-next-line arrow-body-style
export const TransferDetailsStep: FC<TransferDetailsProps> = ({ transaction }) => {
  const timeRemaining = useTransactionCementationTime(Number(transaction.message.level));

  return (
    <Stepper
      currentStep={getStepByStatus(transaction.status)}
      steps={[
        {
          label: 'Transaction on Etherlink',
          description: 'Transaction on Etherlink confirmed.',
        },
        {
          label: 'Waiting for confirmation',
          description: `Transaction is waiting for confirmation. ${transaction.status === TransferStatus.PENDING ? `Estimated time left: ~${timeRemaining}` : ''}`,
        },
        {
          label: 'Execute final withdrawal',
          description: 'Execute final withdrawal on Tezos blockchain.',
          icon: <IconArrowBigDownLines size="1.2rem" />,
        },
      ]}
    />
  );
};
