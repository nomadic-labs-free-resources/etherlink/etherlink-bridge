import React, { FC } from 'react';
import { Group, Table, Text } from '@mantine/core';
import { IconArrowRight } from '@tabler/icons-react';

import { NumberFormatter, TextAddress } from '@/components/ui';

type Props = {
  address?: string;
  amount: number;
  network: 'tezos' | 'etherlink';
  update?: 'increase' | 'decrease';
};

export type TransferDetailsBalanceUpdateProps = Props;
export const TransferDetailsBalanceUpdate: FC<TransferDetailsBalanceUpdateProps> = ({
  address,
  amount,
  network,
  update,
}) => (
  <Table.ScrollContainer minWidth={500}>
    <Table withRowBorders={false}>
      <Table.Thead>
        {address ? (
          <Table.Tr>
            <Table.Th>Address</Table.Th>
            <Table.Th />
            <Table.Th>Balance</Table.Th>
          </Table.Tr>
        ) : (
          <Table.Tr>
            <Table.Th />
          </Table.Tr>
        )}
      </Table.Thead>
      <Table.Tbody>
        <Table.Tr>
          {address && (
            <Table.Td>
              <TextAddress address={address} network={network} />
            </Table.Td>
          )}
          {address && (
            <Table.Td>
              <IconArrowRight size="1.2rem" />
            </Table.Td>
          )}
          <Table.Td>
            {update ? (
              <Group gap="xs" align="center">
                <Text component="span" c={update === 'increase' ? 'green' : 'red'} fw="bold">
                  {update === 'increase' ? '+' : '-'}
                </Text>
                <Text c={update === 'increase' ? 'green' : 'red'}>
                  <NumberFormatter currency="xtz" value={Number(amount)} size="md" />
                </Text>
              </Group>
            ) : (
              <NumberFormatter value={amount} size="md" currency="xtz" />
            )}
          </Table.Td>
        </Table.Tr>
      </Table.Tbody>
    </Table>
  </Table.ScrollContainer>
);
