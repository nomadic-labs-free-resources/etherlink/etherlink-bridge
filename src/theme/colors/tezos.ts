import { MantineColorsTuple } from '@mantine/core';

export const tezos: MantineColorsTuple = [
  '#e5f5ff',
  '#cee5ff',
  '#9cc9ff',
  '#66abfe',
  '#3c92fc',
  '#2382fc',
  '#137afe',
  '#0068e3',
  '#005ccb',
  '#004fb4',
];
