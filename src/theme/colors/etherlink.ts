import { MantineColorsTuple } from '@mantine/core';

export const etherlink: MantineColorsTuple = [
  '#e0fff2',
  '#cbffe5',
  '#99ffcd',
  '#62ffb1',
  '#36ff9b',
  '#18ff8c',
  '#00ff84',
  '#00e370',
  '#00ca62',
  '#00ae51',
];
