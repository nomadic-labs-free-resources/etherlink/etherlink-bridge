import { Button, DEFAULT_THEME, Input, Tabs, createTheme, mergeMantineTheme } from '@mantine/core';
import { etherlink, tezos } from './colors';

import classes from './css/Button.module.css';

export const themeOverride = createTheme({
  scale: 1,
  fontSmoothing: true,
  focusRing: 'auto',
  white: '#fff',
  black: '#000',
  primaryColor: 'etherlink',
  colors: {
    ...DEFAULT_THEME.colors,
    etherlink,
    tezos,
  },
  primaryShade: {
    light: 7,
    dark: 6,
  },
  defaultRadius: 'md',
  headings: { fontFamily: 'Inter, sans-serif' },
  components: {
    Button: Button.extend({ classNames: classes }),
    Tabs: Tabs.extend({
      defaultProps: {
        variant: 'default',
        // color: 'etherlink',
      },
      styles: {
        root: {
          '&[data-active]': {
            background: `linear-gradient(45deg, ${etherlink[5]} 0%, ${tezos[5]} 100%)`,
            borderRadius: '8px',
          },
        },
      },
    }),
    Input: Input.extend({
      defaultProps: {
        variant: 'filled',
        radius: 'md',
      },
      styles: {
        input: {
          border: `1px solid linear-gradient(45deg, ${etherlink[5]} 0%, ${tezos[5]} 100%)`,
          '&:focus': {
            border: `1px solid linear-gradient(45deg, ${etherlink[5]} 0%, ${tezos[5]} 100%)`,
          },
        },
      },
    }),
  },
});

export const theme = mergeMantineTheme(DEFAULT_THEME, themeOverride);
