import { cookieStorage, createConfig, createStorage, http } from 'wagmi';
import { coinbaseWallet, injected, walletConnect } from 'wagmi/connectors';
import { defineChain } from 'viem';

import { env } from '@/env.mjs';

export const projectId = env.NEXT_PUBLIC_ETHERLINK_PROJECT_ID;
if (!projectId) throw new Error('Project ID is not defined');

const metadata = {
  name: 'Etherlink Bridge',
  description: 'Etherlink  Bridge is a decentralized bridge between Etherlink and Tezos network',
  url: env.NEXT_PUBLIC_APP_URL, // origin must match your domain & subdomain
  icons: ['https://etherlink-bridge.netlify.app/etherlink.svg'],
};

export const etherlink = defineChain({
  id: 128123,
  name: 'Etherlink Testnet',
  nativeCurrency: { name: 'Tez', symbol: 'XTZ', decimals: 18 },
  rpcUrls: {
    default: { http: [env.NEXT_PUBLIC_ETHERLINK_RPC_URL] },
  },
  blockExplorers: {
    default: { name: 'Blockscout', url: env.NEXT_PUBLIC_ETHERLINK_EXPLORER_URL },
  },
  contracts: {},
});

export const config = createConfig({
  chains: [etherlink],
  transports: {
    [etherlink.id]: http(),
  },
  connectors: [
    walletConnect({ projectId, metadata, showQrModal: false }),
    injected({ shimDisconnect: true }),
    coinbaseWallet({
      appName: metadata.name,
      appLogoUrl: metadata.icons[0],
    }),
  ],
  ssr: true,
  storage: createStorage({
    storage: cookieStorage,
  }),
});
