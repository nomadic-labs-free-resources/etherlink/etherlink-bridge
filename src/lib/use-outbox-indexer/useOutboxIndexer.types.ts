export type UseGetOutboxIndexerOptions<TQuery> = {
  query?: TQuery;
};

export type UseGetOutboxIndexerReturnType<TResponse> = {
  data: TResponse;
  hasNextPage: boolean;
  isLoading: boolean;
  error: any;
};
