type QueryParamValue =
  | string
  | number
  | Record<string, any>
  | Array<{ orderBy: string; order: string }>;
type QueryParams = Record<string, QueryParamValue>;

export const serializeQueryParams = (params: QueryParams): string => {
  const entries = Object.entries(params).map(([key, value]) => {
    if (typeof value === 'object' && value !== null) {
      // Stringify object or array types
      return `${encodeURIComponent(key)}=${encodeURIComponent(JSON.stringify(value))}`;
    }

    // Directly use string and number types
    return `${encodeURIComponent(key)}=${encodeURIComponent(value.toString())}`;
  });

  // Join all key-value pairs into a query string
  return entries.join('&');
};
