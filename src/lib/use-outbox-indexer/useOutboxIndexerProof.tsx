import { usePostOutboxIndexer } from './useOutboxIndexer';

interface ProofData {
  level: number;
  index: number;
}

export const useOutboxIndexerProof = () => {
  const { post, isLoading, error } = usePostOutboxIndexer<ProofData>('api/v1/transfers');

  const executeProof = async (level: number, index: number) => {
    const data = { level, index };
    const response = await post(data);
    return response;
  };

  return { executeProof, isLoading, error };
};
