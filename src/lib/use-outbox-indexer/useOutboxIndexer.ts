'use client';

import { useState } from 'react';
import useSWR from 'swr';
import axios from 'axios';

// import { env } from '@/env.mjs';
import {
  UseGetOutboxIndexerOptions,
  UseGetOutboxIndexerReturnType,
} from './useOutboxIndexer.types';
import { serializeQueryParams } from './serialize-query-params';

const fetcher = (url: string) =>
  axios
    .get(url, {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      },
    })
    .then((res) => res.data);

export const useGetOutboxIndexer = <TQuery, TResponse>(
  endpoint: string,
  options?: UseGetOutboxIndexerOptions<TQuery>
): UseGetOutboxIndexerReturnType<TResponse> => {
  // const apiBaseUrl = env.NEXT_PUBLIC_OUTBOX_INDEXER_URL || 'http://localhost:3000';
  const apiBaseUrl = '/api';
  const queryString = options && options.query ? `?${serializeQueryParams(options.query)}` : '';
  const url = `${apiBaseUrl}/${endpoint}${queryString}`;

  const { data, isLoading, error } = useSWR(url, fetcher);

  return {
    data: data?.data,
    hasNextPage: data?.hasNextPage ?? false,
    isLoading,
    error,
  };
};

export type UsePostOutboxIndexerReturnType<T> = {
  post: (data: T) => Promise<void>;
  isLoading: boolean;
  error: any;
};

/**
 * usePostOutboxIndexer - a hook to perform POST requests with Axios.
 *
 * @param {string} url The URL to post the data to.
 * @returns An object containing the postData function, loading state, and any error.
 */
export const usePostOutboxIndexer = <T>(endpoint: string): UsePostOutboxIndexerReturnType<T> => {
  // const apiBaseUrl = env.NEXT_PUBLIC_OUTBOX_INDEXER_URL || 'http://localhost:3000';
  const apiBaseUrl = '/api';
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  const post = async (data: T) => {
    setIsLoading(true);
    setError(null);

    try {
      const response = await axios.post(`${apiBaseUrl}/${endpoint}`, data);
      setIsLoading(false);
      return response.data;
    } catch (err) {
      setIsLoading(false);
      setError(err as any);
      throw err;
    }
  };

  return { post, isLoading, error };
};
