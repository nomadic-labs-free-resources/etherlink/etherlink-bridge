'use client';

import { useGetOutboxIndexer } from './useOutboxIndexer';
import { UseGetOutboxIndexerOptions } from './useOutboxIndexer.types';

export enum TransferType {
  DEPOSIT = 'DEPOSIT',
  WITHDRAW = 'WITHDRAW',
}

export enum TransferStatus {
  PENDING = 'PENDING',
  READY = 'READY',
  CONFIRMED = 'CONFIRMED',
  FAILED = 'FAILED',
}

export type TransactionMessage = {
  level: string;
  index: string;
};

export type Transaction = {
  id: number;
  type: TransferType;
  status: TransferStatus;
  sender: string;
  receiver: string;
  amount: string;
  tezosTimestamp: string;
  tezosOperationLevel: string;
  tezosOperationHash: string;
  tezosOperationIndex: string;
  etherlinkTimestamp: string;
  etherlinkOperationLevel: string;
  etherlinkOperationHash: string;
  etherlinkOperationIndex: string;
  message: TransactionMessage;
};

export type UseOutboxIndexerTransactionsOptions = UseGetOutboxIndexerOptions<{
  page?: number;
  limit?: number;
  filters?: Record<string, string>;
  sort?: Array<{ orderBy: string; order: string }>;
}>;

export type UseOutboxIndexerReturnType = {
  transactions: Transaction[];
  hasNextPage: boolean;
  isLoading: boolean;
  isError: any;
};

export function useOutboxIndexerTransactions(
  options?: UseOutboxIndexerTransactionsOptions
): UseOutboxIndexerReturnType {
  const { data, hasNextPage, isLoading, error } = useGetOutboxIndexer<
    UseOutboxIndexerTransactionsOptions['query'],
    Transaction[]
  >('api/v1/messages', options);

  return {
    transactions: data || ([] as Transaction[]),
    hasNextPage,
    isLoading,
    isError: error,
  };
}

export function useIndexerTransfers(
  options?: UseOutboxIndexerTransactionsOptions
): UseOutboxIndexerReturnType {
  const { data, hasNextPage, isLoading, error } = useGetOutboxIndexer<
    UseOutboxIndexerTransactionsOptions['query'],
    Transaction[]
  >('api/v1/transfers', options);

  return {
    transactions: data || ([] as Transaction[]),
    hasNextPage,
    isLoading,
    isError: error,
  };
}
