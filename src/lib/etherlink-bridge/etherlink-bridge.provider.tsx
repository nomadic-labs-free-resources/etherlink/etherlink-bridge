import React, { createContext, FC, PropsWithChildren, useContext } from 'react';
import { Config as WagmiConfig, WagmiProvider } from 'wagmi';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { TezosProvider } from '@nlfr/use-tezos';
import type { TezosConfig } from '@nlfr/use-tezos';

interface EtherlinkBridgeProviderProps {
  tezosConfig?: TezosConfig;
  wagmiConfig?: WagmiConfig;
}

const EtherlinkBridgeContext = createContext<EtherlinkBridgeProviderProps | undefined>(undefined);

export const EtherlinkBridgeProvider: FC<PropsWithChildren<EtherlinkBridgeProviderProps>> = ({
  children,
  tezosConfig,
  wagmiConfig,
}) => {
  const queryClient = new QueryClient();

  return (
    <EtherlinkBridgeContext.Provider value={{ tezosConfig, wagmiConfig }}>
      {tezosConfig ? (
        <TezosProvider config={tezosConfig.config}>
          {wagmiConfig ? (
            <WagmiProvider config={wagmiConfig}>
              <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
            </WagmiProvider>
          ) : (
            children
          )}
        </TezosProvider>
      ) : wagmiConfig ? (
        <WagmiProvider config={wagmiConfig}>
          <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
        </WagmiProvider>
      ) : (
        children
      )}
    </EtherlinkBridgeContext.Provider>
  );
};

export const useEtherlinkBridge = () => {
  const context = useContext(EtherlinkBridgeContext);
  if (!context) {
    throw new Error('useEtherlinkBridge must be used within a EtherlinkBridgeProvider');
  }
  return context;
};
