import { useCallback } from 'react';
import { TransactionWalletOperation } from '@taquito/taquito';
import { useTezos } from '@nlfr/use-tezos';

import { tas } from '../../types/type-aliases';
import { useEtherlinkBridge } from '../etherlink-bridge.provider';

export type UseEtherlinkBridgeDepositArgs = {
  evmAddress: string;
  amount: number;
  bridgeContractAddress: string;
  rollupContractAddress: string;
  mutez?: boolean;
};

export type UseEtherlinkBridgeDeposit = (
  args: UseEtherlinkBridgeDepositArgs
) => Promise<TransactionWalletOperation>;

export const useDeposit = () => {
  useEtherlinkBridge();
  const { toolkit } = useTezos();

  const deposit: UseEtherlinkBridgeDeposit = useCallback(
    async ({ evmAddress, amount, bridgeContractAddress, rollupContractAddress, mutez = false }) => {
      if (!toolkit) {
        throw new Error('Wallet is not available.');
      }

      const bridgeContract = await toolkit.wallet.at(bridgeContractAddress);
      const operation = await bridgeContract.methodsObject
        .deposit({
          l2_address: tas.bytes(evmAddress),
          evm_address: tas.address(rollupContractAddress),
        })
        .send({ amount, mutez });

      return operation;
    },
    [toolkit]
  );

  return deposit;
};
