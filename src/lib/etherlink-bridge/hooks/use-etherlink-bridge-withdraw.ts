import { useCallback } from 'react';
import { useAccount, useWriteContract } from 'wagmi';
import { parseEther } from 'viem';

import { useEtherlinkBridge } from '../etherlink-bridge.provider';
import { ABI } from '../artefacts/abi';

export type UseEtherlinkBridgeWithdrawArgs = {
  tzAddress: string;
  amount: number;
  bridgeContractAddress: string;
};

export type UseEtherlinkBridgeWithdraw = (
  args: UseEtherlinkBridgeWithdrawArgs
) => Promise<`0x${string}`>;

export const useWithdraw = () => {
  useEtherlinkBridge();
  const account = useAccount();
  const { writeContractAsync } = useWriteContract();

  const withdraw: UseEtherlinkBridgeWithdraw = useCallback(
    async ({ tzAddress, amount, bridgeContractAddress }) => {
      if (account.isDisconnected) {
        throw new Error('Wallet is not available.');
      }

      const operation = await writeContractAsync({
        abi: ABI,
        address: bridgeContractAddress as `0x${string}`,
        functionName: 'withdraw_base58',
        args: [tzAddress],
        value: parseEther(amount.toString()),
      });

      return operation;
    },
    [account]
  );

  return withdraw;
};
