import { TezosToolkit } from '@taquito/taquito';
import { env } from '@/env.mjs';
import { EvmBridgeWalletType } from '../types/evm_bridge.types';
import { tas } from '../types/type-aliases';
import { BridgeDeposit } from './etherlink-bridge-sdk.types';

export class EtherlinkBridgeSDK {
  private readonly toolkit: TezosToolkit;

  constructor(toolkit: TezosToolkit) {
    if (!toolkit) {
      throw new Error('Tezos toolkit is required');
    }
    this.toolkit = toolkit;
  }

  async deposit({
    evmAddress,
    amount,
    mutez = false,
    bridgeContractAddress = env.NEXT_PUBLIC_TZ_BRIDGE_KT,
    rollupContractAddress = env.NEXT_PUBLIC_TZ_ROLLUP_SR,
  }: BridgeDeposit) {
    const bridgeContract = await this.toolkit.wallet.at<EvmBridgeWalletType>(bridgeContractAddress);
    const operation = await bridgeContract.methodsObject
      .deposit({
        l2_address: tas.bytes(evmAddress),
        evm_address: tas.address(rollupContractAddress),
      })
      .send({ amount, mutez });
    return operation;
  }
}
