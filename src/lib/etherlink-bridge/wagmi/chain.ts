import { defineChain } from 'viem';

import { env } from '@/env.mjs';

export const etherlinkChain =
  env.NEXT_PUBLIC_TZ_NETWORK === 'mainnet'
    ? defineChain({
        id: 42793,
        name: 'Etherlink',
        nativeCurrency: { name: 'Tez', symbol: 'XTZ', decimals: 18 },
        rpcUrls: {
          default: { http: [env.NEXT_PUBLIC_ETHERLINK_RPC_URL] },
        },
        blockExplorers: {
          default: { name: 'Blockscout', url: env.NEXT_PUBLIC_ETHERLINK_EXPLORER_URL },
        },
        contracts: {},
      })
    : defineChain({
        id: 128123,
        name: 'Etherlink Testnet',
        nativeCurrency: { name: 'Tez', symbol: 'XTZ', decimals: 18 },
        rpcUrls: {
          default: { http: [env.NEXT_PUBLIC_ETHERLINK_RPC_URL] },
        },
        blockExplorers: {
          default: { name: 'Blockscout', url: env.NEXT_PUBLIC_ETHERLINK_EXPLORER_URL },
        },
        contracts: {},
      });
