import { cookieStorage, createConfig, createStorage, http } from 'wagmi';
import { coinbaseWallet, injected, walletConnect } from 'wagmi/connectors';

import { env } from '@/env.mjs';
import { etherlinkChain } from './chain';

const metadata = {
  name: 'Etherlink Bridge',
  description: 'Etherlink  Bridge is a decentralized bridge between Etherlink and Tezos network',
  url: env.NEXT_PUBLIC_APP_URL, // origin must match your domain & subdomain
  icons: ['https://bridge.etherlink.com/assets/etherlink.svg'],
};

export const etherlinkConfig = createConfig({
  chains: [etherlinkChain],
  transports: {
    42793: http(),
    128123: http(),
  },
  connectors: [
    walletConnect({
      projectId: env.NEXT_PUBLIC_ETHERLINK_PROJECT_ID,
      metadata,
      showQrModal: false,
    }),
    injected({ shimDisconnect: true }),
    coinbaseWallet({
      appName: metadata.name,
      appLogoUrl: metadata.icons[0],
    }),
  ],
  ssr: true,
  storage: createStorage({
    storage: cookieStorage,
  }),
});
