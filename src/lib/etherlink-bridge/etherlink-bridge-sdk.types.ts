import { TezosConfig } from '@nlfr/use-tezos';
import { Config } from 'wagmi';

export interface UseEtherlinkBridge {
  deposit: (args: any) => Promise<void>;
  writeContractAsync: (args: any) => Promise<void>;
}

export interface EtherlinkBridgeConfig {
  tezos?: TezosConfig;
  wagmi?: Config;
}

export type BridgeDeposit = {
  evmAddress: string;
  amount: number;
  mutez?: boolean;
  bridgeContractAddress?: string;
  rollupContractAddress?: string;
};
