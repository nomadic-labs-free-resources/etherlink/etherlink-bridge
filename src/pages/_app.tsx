import '@mantine/core/styles.css';
import '@mantine/notifications/styles.css';

import React from 'react';
import { AppProps } from 'next/app';
import Head from 'next/head';
import { MantineProvider } from '@mantine/core';

import { theme } from '@/theme/theme';
import { RootLayout } from '@/layouts/Root';

import '@/theme/css/globals.css';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Etherlink Bridge</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
        <link rel="shortcut icon" href="/favicon.ico" />
      </Head>
      <MantineProvider
        withCssVariables
        withGlobalClasses
        withStaticClasses
        theme={theme}
        defaultColorScheme="dark"
      >
        <RootLayout>
          <Component {...pageProps} />
        </RootLayout>
      </MantineProvider>
    </>
  );
}
