'use client';

import { Box, Center, Container, Flex, Text, Title } from '@mantine/core';

import { terms } from '@/utils/terms';

export default function TermsPage() {
  const containsLetterInParentheses = (paragraph: string) => {
    const regex = /\([a-zA-Z]\)/;
    return regex.test(paragraph);
  };

  return (
    <Box>
      <Container size="sm" my="md">
        {/*  */}
        <Center>
          <Flex align="center" direction="column">
            <Title size="h3" mt={20} fw="bolder" variant="gradient">
              <Text inherit component="span">
                Etherlink Bridge
              </Text>
              &nbsp;Terms and Conditions
            </Title>
            {terms.map((term, index) => (
              <Container mt={30} key={index}>
                <Text fw={!term.title ? '' : 'bold'}>{term.title}</Text>
                <Center>
                  <Text fw={!term.subPart ? '' : 'bold'} mt={15} mb={10}>
                    {term.subPart}
                  </Text>
                </Center>
                {/* <Text mt={10} mb={10}>{term.subTitle}</Text> */}
                {term.paragraphs.map((paragraph, pindex) => (
                  <Text
                    key={pindex}
                    style={{ textAlign: 'justify' }}
                    ml={containsLetterInParentheses(paragraph) ? 60 : 0}
                  >
                    {' '}
                    {paragraph === '</br>' ? <br /> : paragraph}
                  </Text>
                ))}
              </Container>
            ))}
          </Flex>
        </Center>
      </Container>
    </Box>
  );
}
