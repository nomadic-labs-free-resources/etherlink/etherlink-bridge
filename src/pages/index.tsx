'use client';

import { useState } from 'react';
import { Box, Card, Container } from '@mantine/core';

import { env } from '@/env.mjs';
import { TabsSegmented, BetaDisclaimer } from '@/components';
import { Deposit, Withdraw } from '@/features';

export default function HomePage() {
  const [selectedTitle, setSelectedTitle] = useState('Deposit');

  return (
    <Box>
      <BetaDisclaimer />
      <Container size="sm" my="md">
        <Card withBorder>
          <TabsSegmented
            activeTab={selectedTitle}
            tabs={[
              { title: 'Deposit', component: <Deposit onTabChange={setSelectedTitle} /> },
              {
                title: 'Withdraw',
                component: <Withdraw onTabChange={setSelectedTitle} />,
                disabled: !env.NEXT_PUBLIC_APP_WITHDRAW_ENABLED,
              },
            ]}
            onTabChange={setSelectedTitle}
          />
        </Card>
      </Container>
    </Box>
  );
}
