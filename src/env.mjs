import { createEnv } from '@t3-oss/env-nextjs';
import { z } from 'zod';

export const env = createEnv({
  /**
   * Server-side environment variables schema.
   * NOT AVAILABLE ON THE CLIENT.
   */
  server: {
    NODE_ENV: z.enum(['development', 'test', 'production']),
  },

  /**
   * Client-side (& server-side) environment variables schema.
   * MUST BE PREFIXED WITH `NEXT_PUBLIC_`.
   */
  client: {
    NEXT_PUBLIC_APP_VERSION: z.string(),
    NEXT_PUBLIC_APP_GIT: z.string(),
    NEXT_PUBLIC_APP_URL: z.string(),
    NEXT_PUBLIC_APP_THEME: z.enum(['tezos', 'etherlink']).default('tezos'),
    NEXT_PUBLIC_APP_WITHDRAW_ENABLED: z.boolean().default(false),
    NEXT_PUBLIC_APP_BRIDGE_AMOUNT_LIMIT: z.number().default(100),
    NEXT_PUBLIC_APP_SWITCH_URL: z.string().url().optional(),

    NEXT_PUBLIC_TZ_NETWORK: z.enum(['mainnet', 'ghostnet', 'sandbox']).default('ghostnet'),
    NEXT_PUBLIC_TZ_RPC_URL: z.string(),
    NEXT_PUBLIC_TZ_BRIDGE_KT: z.string().startsWith('KT1').length(36),
    NEXT_PUBLIC_TZ_ROLLUP_SR: z.string().startsWith('sr1').length(36),
    NEXT_PUBLIC_TZ_EXPLORER_URL: z.string(),

    NEXT_PUBLIC_ETHERLINK_PROJECT_ID: z.string(),
    NEXT_PUBLIC_ETHERLINK_RPC_URL: z.string(),
    NEXT_PUBLIC_ETHERLINK_EXPLORER_URL: z.string(),
    NEXT_PUBLIC_ETHERLINK_BRIDGE_0X: z.string(),

    NEXT_PUBLIC_OUTBOX_INDEXER_URL: z.string(),

    NEXT_PUBLIC_LINK_ETHERLINK_X: z.string(),
    NEXT_PUBLIC_LINK_ETHERLINK_DISCORD: z.string(),
    NEXT_PUBLIC_LINK_ETHERLINK_TELEGRAM: z.string(),
    NEXT_PUBLIC_LINK_ETHERLINK_GITHUB: z.string(),

    NEXT_PUBLIC_DISCLAIMER: z.string().default(''),
    NEXT_PUBLIC_DISCLAIMER_POPUP_ENABLED: z.boolean().default(false),
    NEXT_PUBLIC_DISCLAIMER_COPYRIGHT: z.string(),
  },

  /**
   * You can't destruct `process.env` as a regular object in the Next.js edge runtimes (e.g.
   * middlewares) or client-side so we need to destruct manually.
   *
   * All variables from `server` & `client` must be included here.
   */
  runtimeEnv: {
    NODE_ENV: process.env.NODE_ENV,

    NEXT_PUBLIC_APP_VERSION: process.env.NEXT_PUBLIC_APP_VERSION,
    NEXT_PUBLIC_APP_GIT: process.env.NEXT_PUBLIC_APP_GIT,
    NEXT_PUBLIC_APP_URL: process.env.NEXT_PUBLIC_APP_URL,
    NEXT_PUBLIC_APP_THEME: process.env.NEXT_PUBLIC_APP_THEME,
    NEXT_PUBLIC_APP_WITHDRAW_ENABLED: process.env.NEXT_PUBLIC_APP_WITHDRAW_ENABLED === 'true',
    NEXT_PUBLIC_APP_BRIDGE_AMOUNT_LIMIT: process.env.NEXT_PUBLIC_APP_BRIDGE_AMOUNT_LIMIT
      ? parseInt(process.env.NEXT_PUBLIC_APP_BRIDGE_AMOUNT_LIMIT)
      : undefined,
    NEXT_PUBLIC_APP_SWITCH_URL:
      process.env.NEXT_PUBLIC_APP_SWITCH_URL || process.env.NEXT_PUBLIC_TZ_NETWORK === 'mainnet'
        ? 'https://testnet.bridge.etherlink.com/'
        : 'https://bridge.etherlink.com/',

    NEXT_PUBLIC_TZ_NETWORK: process.env.NEXT_PUBLIC_TZ_NETWORK,
    NEXT_PUBLIC_TZ_RPC_URL: process.env.NEXT_PUBLIC_TZ_RPC_URL,
    NEXT_PUBLIC_TZ_BRIDGE_KT: process.env.NEXT_PUBLIC_TZ_BRIDGE_KT,
    NEXT_PUBLIC_TZ_ROLLUP_SR: process.env.NEXT_PUBLIC_TZ_ROLLUP_SR,
    NEXT_PUBLIC_TZ_EXPLORER_URL: process.env.NEXT_PUBLIC_TZ_EXPLORER_URL,

    NEXT_PUBLIC_ETHERLINK_PROJECT_ID: process.env.NEXT_PUBLIC_ETHERLINK_PROJECT_ID,
    NEXT_PUBLIC_ETHERLINK_RPC_URL: process.env.NEXT_PUBLIC_ETHERLINK_RPC_URL,
    NEXT_PUBLIC_ETHERLINK_EXPLORER_URL: process.env.NEXT_PUBLIC_ETHERLINK_EXPLORER_URL,
    NEXT_PUBLIC_ETHERLINK_BRIDGE_0X: process.env.NEXT_PUBLIC_ETHERLINK_BRIDGE_0X,

    NEXT_PUBLIC_OUTBOX_INDEXER_URL: process.env.NEXT_PUBLIC_OUTBOX_INDEXER_URL,

    NEXT_PUBLIC_LINK_ETHERLINK_X: process.env.NEXT_PUBLIC_LINK_ETHERLINK_X,
    NEXT_PUBLIC_LINK_ETHERLINK_DISCORD: process.env.NEXT_PUBLIC_LINK_ETHERLINK_DISCORD,
    NEXT_PUBLIC_LINK_ETHERLINK_TELEGRAM: process.env.NEXT_PUBLIC_LINK_ETHERLINK_TELEGRAM,
    NEXT_PUBLIC_LINK_ETHERLINK_GITHUB: process.env.NEXT_PUBLIC_LINK_ETHERLINK_GITHUB,

    NEXT_PUBLIC_DISCLAIMER: process.env.NEXT_PUBLIC_DISCLAIMER,
    NEXT_PUBLIC_DISCLAIMER_POPUP_ENABLED:
      process.env.NEXT_PUBLIC_DISCLAIMER_POPUP_ENABLED === 'true',
    NEXT_PUBLIC_DISCLAIMER_COPYRIGHT: process.env.NEXT_PUBLIC_DISCLAIMER_COPYRIGHT,
  },
  /**
   * Run `build` or `dev` with `SKIP_ENV_VALIDATION` to skip env validation. This is especially
   * useful for Docker builds.
   */
  skipValidation: !!process.env.SKIP_ENV_VALIDATION,
  /**
   * Makes it so that empty strings are treated as undefined. `SOME_VAR: z.string()` and
   * `SOME_VAR=''` will throw an error.
   */
  emptyStringAsUndefined: true,
});
