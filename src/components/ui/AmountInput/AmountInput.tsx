'use client';

import { NumberInput as MantineNumberInput } from '@mantine/core';

import { FC } from 'react';
import { UseFormReturnType } from '@mantine/form';
import { NumberInputCurrencyRight } from './AmountInputRight';

type Currency = 'xtz';

type Props = {
  form: UseFormReturnType<any>;
  inputPropsKey: string;
  currency: Currency;
};

export const NumberInput: FC<Props> = (props) => (
  <MantineNumberInput
    placeholder="0"
    error={props.form.errors[props.inputPropsKey]}
    errorProps={{ fw: 'normal', fz: 'xs' }}
    size="lg"
    hideControls
    decimalScale={6}
    rightSection={<NumberInputCurrencyRight currency={props.currency} />}
    rightSectionWidth={150}
    fw="bolder"
    {...props.form.getInputProps(props.inputPropsKey)}
  />
);
