'use client';

import { Text, useMantineTheme, Center, rem } from '@mantine/core';
import { useColorScheme } from '@mantine/hooks';

import { FC } from 'react';

type Props = {
  currency: string;
};

export const NumberInputCurrencyRight: FC<Props> = (props) => {
  const { colors } = useMantineTheme();
  const colorScheme = useColorScheme();

  return (
    <Center
      style={{
        borderLeft: `1px solid ${colorScheme === 'light' ? colors.gray[4] : colors.red[7]}`,
        fontWeight: 700,
        width: rem(100),
        height: rem(50),
      }}
    >
      <Text size="lg" fw="bolder">
        {props.currency.toUpperCase()}
      </Text>
    </Center>
  );
};
