import React, { FC } from 'react';
import { Drawer as MantineDrawer, DrawerProps as MantineDrawerProps, Text } from '@mantine/core';

type Props = {
  title: string;
};

export type DrawerProps = Props & MantineDrawerProps;
export const Drawer: FC<DrawerProps> = ({ children, ...rest }) => (
  <MantineDrawer
    title={
      <Text size="xl" fw="bold" fs="italic">
        {rest.title}
      </Text>
    }
    overlayProps={{ backgroundOpacity: 0.5, blur: 4 }}
    size="xl"
    position="right"
    radius="md"
    offset={8}
    opened={rest.opened}
    onClose={rest.onClose}
  >
    {children}
  </MantineDrawer>
);
