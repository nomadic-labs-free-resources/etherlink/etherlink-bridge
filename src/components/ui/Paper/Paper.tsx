import {
  Paper as MantinePaper,
  PaperProps as MantinePaperProps,
  useMantineColorScheme,
  useMantineTheme,
} from '@mantine/core';
import { FC, PropsWithChildren } from 'react';

type Props = MantinePaperProps;

export type PaperProps = Props;
export const Paper: FC<PropsWithChildren<Props>> = ({ children, ...props }) => {
  const { colors } = useMantineTheme();
  const { colorScheme } = useMantineColorScheme();

  return (
    <MantinePaper p="xs" bg={colorScheme === 'dark' ? colors.dark[8] : colors.gray[0]} {...props}>
      {children}
    </MantinePaper>
  );
};
