import React, { PropsWithChildren } from 'react';
import { Modal as MantineModal, Text, ModalBody, Box, Group, ScrollArea } from '@mantine/core';

interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  title?: string | { title: string; date: string; status: React.ReactNode };
  modalRadius?: number;
}

export const Modal: React.FC<PropsWithChildren<ModalProps>> = ({
  children,
  isOpen,
  onClose,
  title,
  modalRadius,
}) => (
  <MantineModal
    radius={modalRadius}
    size="auto"
    centered
    scrollAreaComponent={ScrollArea.Autosize}
    opened={isOpen}
    onClose={onClose}
    title={
      typeof title === 'string' ? (
        <Text size="xl" fw="bold" fs="italic">
          {title}
        </Text>
      ) : (
        <Box>
          <Text size="xl" fw="bold" fs="italic">
            {title?.title}
          </Text>
          <Group>
            <Text fz="sm" c="dimmed" fw="bold">
              {title?.date}
            </Text>
            <Box>{title?.status}</Box>
          </Group>
        </Box>
      )
    }
  >
    <ModalBody>{children}</ModalBody>
  </MantineModal>
);
