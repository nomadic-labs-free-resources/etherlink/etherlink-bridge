import React from 'react';
import { Button, ButtonProps } from '@mantine/core';

// import classes from './ButtonGradient.module.css';

export type ButtonGradientProps = ButtonProps & React.ComponentPropsWithoutRef<'button'>;
export function ButtonGradient(props: ButtonGradientProps) {
  return (
    <Button
      {...props}
      variant="default"
      radius="md"
      // classNames={{ root: classes.root, inner: classes.inner, label: classes.label }}
    />
  );
}
