import { ActionIcon, useMantineColorScheme, useMantineTheme } from '@mantine/core';
import { IconSun, IconMoonStars } from '@tabler/icons-react';

export function ColorSchemeToggle() {
  const { colorScheme, toggleColorScheme } = useMantineColorScheme();
  const { colors } = useMantineTheme();

  return (
    <ActionIcon
      onClick={() => toggleColorScheme()}
      color={colorScheme === 'dark' ? colors.dark[6] : colors.gray[0]}
    >
      {colorScheme === 'dark' ? (
        <IconSun size="1.2rem" stroke={1.5} color={colors.etherlink[6]} />
      ) : (
        <IconMoonStars size="1.2rem" stroke={1.5} color={colors.etherlink[7]} />
      )}
    </ActionIcon>
  );
}
