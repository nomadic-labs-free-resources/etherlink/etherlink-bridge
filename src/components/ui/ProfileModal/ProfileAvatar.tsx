import { shortEvmAddress } from '@/utils/utils';
import {
  Group,
  Center,
  CopyButton,
  ActionIcon,
  Tooltip,
  rem,
  Avatar,
  Text,
  Flex,
} from '@mantine/core';
import { IconCopy, IconCheck } from '@tabler/icons-react';
import { TezosWalletBalance } from '../../common/TezosWalletBalance/TezosWalletBalance';
import { EtherlinkWalletBalance } from '../../common/EtherlinkWalletBalance/EtherlinkWalletBalance';

interface AvatarProps {
  address: string;
  walletType: string;
  advanced: boolean;
}

export const ProfileAvatar: React.FC<AvatarProps> = ({ address, walletType, advanced }) => (
  <>
    {/* With image */}
    <Flex mih={50} gap="md" justify="center" align="center" direction="column">
      <Avatar
        src={walletType === 'tezos' ? '/assets/cover_1.jpg' : '/assets/etherlink.svg'}
        alt="it's me"
        size={60}
      />
      <Group>
        <Center>
          <Text fw={700} size="xl">
            {shortEvmAddress(address)}
          </Text>
        </Center>
        <CopyButton value={address} timeout={1000}>
          {({ copied, copy }) => (
            <Tooltip label={copied ? 'Copied' : 'Copy'} withArrow position="right">
              <ActionIcon color={copied ? 'teal' : 'gray'} variant="subtle" onClick={copy}>
                {copied ? (
                  <IconCheck style={{ width: rem(16) }} />
                ) : (
                  <IconCopy style={{ width: rem(16) }} />
                )}
              </ActionIcon>
            </Tooltip>
          )}
        </CopyButton>
      </Group>
      {!advanced ? (
        walletType === 'tezos' ? (
          <TezosWalletBalance size="22" />
        ) : (
          <EtherlinkWalletBalance size="22" />
        )
      ) : null}
    </Flex>
  </>
);
