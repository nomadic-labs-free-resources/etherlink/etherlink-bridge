import { useForm } from '@mantine/form';
import { Button, NumberInput, Text } from '@mantine/core';
import { IconHammer } from '@tabler/icons-react';
import { useExecuteWithdraw } from '@/hooks';
import classes from './ExecutionForm.module.css';

export type ProofExecutionForm = {
  outboxIndex: number;
  outboxLevel: number;
};

export const ExecutionForm = () => {
  const { executeTransaction, isLoading } = useExecuteWithdraw();

  const form = useForm<ProofExecutionForm>({
    initialValues: {
      outboxIndex: 0,
      outboxLevel: 0,
    },
    validate: {
      outboxIndex: (val) => (val >= 0 ? null : 'Invalid outbox index'),
      outboxLevel: (val) => (val >= 0 ? null : 'Invalid outbox level'),
    },
    validateInputOnChange: true,
  });

  const handleSubmit = async () => {
    if (form.isValid()) {
      await executeTransaction(
        form.values.outboxLevel.toString(),
        form.values.outboxIndex.toString()
      );
    }
  };

  return (
    <>
      <Text fw={500} size="md">
        Proof execution
      </Text>
      <NumberInput
        label="Outbox index"
        placeholder="0"
        error={form.errors.outboxIndex}
        errorProps={{ fw: 'normal', fz: 'xs' }}
        size="lg"
        hideControls
        w={300}
        fw="bolder"
        {...form.getInputProps('outboxIndex')}
        classNames={classes}
      />
      <NumberInput
        label="Outbox level"
        placeholder="0"
        error={form.errors.outboxLevel}
        errorProps={{ fw: 'normal', fz: 'xs' }}
        size="lg"
        hideControls
        w={300}
        fw="bolder"
        {...form.getInputProps('outboxLevel')}
        classNames={classes}
      />
      <Button
        variant="light"
        w={300}
        rightSection={<IconHammer size="1.2rem" />}
        onClick={handleSubmit}
        loading={isLoading}
      >
        Execute proof
      </Button>
    </>
  );
};
