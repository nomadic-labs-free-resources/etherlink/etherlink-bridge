import React, { useState } from 'react';
import { Button, Flex } from '@mantine/core';
import { /* IconAdjustments, */ IconExternalLink, IconPlugConnectedX } from '@tabler/icons-react';
import { TezosIcon } from '@/icons/TezosIcon/TezosIcon';
import { EthIcon } from '@/icons/EthIcon';
import { env } from '@/env.mjs';
import { useDisconnect } from 'wagmi';
import { useAccount } from '@nlfr/use-tezos';
import { ProfileAvatar } from './ProfileAvatar';
import { ExecutionForm } from './ExecutionForm';
import { Modal } from '../Modal/Modal';

interface ProfileModalProps {
  isOpen: boolean;
  closeIt: () => void;
  address: string;
  walletType: string;
}

export const ProfileModal: React.FC<ProfileModalProps> = ({
  isOpen,
  closeIt,
  address,
  walletType,
}) => {
  const [advanced] = useState(false);
  const { disconnect } = useDisconnect();
  const { disconnect: tezosDisconnect } = useAccount();

  const handleDisconnectClick = async () => {
    if (walletType === 'tezos') {
      closeIt();
      tezosDisconnect();
    } else {
      closeIt();
      disconnect();
    }
  };

  const WalletIcon = walletType === 'tezos' ? TezosIcon : EthIcon;
  const networkUrl =
    walletType === 'tezos'
      ? env.NEXT_PUBLIC_TZ_EXPLORER_URL
      : `${env.NEXT_PUBLIC_ETHERLINK_EXPLORER_URL}/${'address'}`;
  return (
    <Modal isOpen={isOpen} onClose={closeIt} modalRadius={20}>
      <Flex mih={50} gap="md" justify="center" align="center" direction="column">
        <ProfileAvatar address={address} walletType={walletType} advanced={advanced} />
        {!advanced ? (
          <>
            <Button
              variant="default"
              color="gray"
              justify="space-between"
              w={300}
              leftSection={<WalletIcon />}
              rightSection={<IconExternalLink size="1.2rem" />}
              onClick={() => window.open(`${networkUrl}/${address}`)}
            >
              Block Explorer
            </Button>
            <Button
              variant="default"
              color="gray"
              justify="space-between"
              w={300}
              leftSection={<WalletIcon />}
              rightSection={<IconPlugConnectedX size="1.2rem" />}
              onClick={() => handleDisconnectClick()}
            >
              Disconnect
            </Button>
          </>
        ) : (
          <ExecutionForm />
        )}
        {/* <Flex>
          <Button
            variant="subtle"
            color="gray"
            leftSection={<IconAdjustments />}
            onClick={() => {
              advanced ? setAdvanced(false) : setAdvanced(true);
            }}
          >
            {advanced ? 'Regular mode' : 'Advanced Mode'}
          </Button>
        </Flex> */}
      </Flex>
    </Modal>
  );
};
