import React, { FC, useState } from 'react';
import { Tabs as MantineTabs, Text } from '@mantine/core';

export type Props = {
  tabs: {
    label: string;
    value: string;
    content: JSX.Element;
    disabled?: boolean;
  }[];
};

export const Tabs: FC<Props> = ({ tabs }) => {
  const [activeTab, setActiveTab] = useState<string | null>(tabs[0].value);

  return (
    <MantineTabs value={activeTab} onChange={setActiveTab} keepMounted={false}>
      <MantineTabs.List grow>
        {tabs.map((tab) => (
          <MantineTabs.Tab key={`tabs-tab-${tab.value}`} value={tab.value} disabled={tab.disabled}>
            <Text size="md" fw={700}>
              {tab.label}
            </Text>
          </MantineTabs.Tab>
        ))}
      </MantineTabs.List>

      {tabs.map((tab) => (
        <MantineTabs.Panel value={tab.value} key={tab.value} pt="xs">
          {tab.content}
        </MantineTabs.Panel>
      ))}
    </MantineTabs>
  );
};
