'use client';

import { FC } from 'react';
import { MantineSize, NumberFormatter as MantineNumberFormatter, Text } from '@mantine/core';

import classes from './NumberFormatter.module.css';

type Currency = 'xtz' | 'eth';

type Props = {
  value: number;
  currency: Currency;
  size: MantineSize;
  weight?: 'bold' | 600;
};

export type NumberFormatterProps = Props;
export const NumberFormatter: FC<NumberFormatterProps> = ({
  currency,
  value,
  size,
  weight = 'bold',
}) => (
  <Text className={classes.value} size={size} fw={weight}>
    <MantineNumberFormatter
      prefix={currency === 'xtz' ? 'ꜩ ' : 'Ξ '}
      thousandSeparator=" "
      decimalSeparator="."
      allowNegative={false}
      value={value}
    />
  </Text>
);
