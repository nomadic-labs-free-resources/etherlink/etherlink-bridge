import { FC, PropsWithChildren } from 'react';
import { ActionIcon, ActionIconProps, useMantineTheme } from '@mantine/core';

import classes from './InteractiveIcon.module.css';

type Props = {
  onClick?: () => void;
  Icon: React.ComponentType<React.SVGProps<SVGSVGElement>>;
};

export type InteractiveIconProps = ActionIconProps & Props;
export const InteractiveIcon: FC<PropsWithChildren<InteractiveIconProps>> = ({
  onClick,
  Icon,
  disabled,
}) => {
  const theme = useMantineTheme();

  if (disabled) {
    return <Icon color={theme.colors.etherlink[7]} />;
  }

  return (
    <ActionIcon onClick={() => onClick?.()} variant="transparent">
      <Icon className={classes.InteractiveIcon} color={theme.colors.etherlink[7]} />
    </ActionIcon>
  );
};
