import React, { FC } from 'react';
import {
  Stepper as MantineStepper,
  StepperStepProps as MantineStepperProps,
  Text,
  useMantineTheme,
} from '@mantine/core';
import { IconCircleCheck, IconClockHour2 } from '@tabler/icons-react';

type Step = {
  label: string;
  description: string;
  icon?: React.ReactNode;
};

type StepperProps = MantineStepperProps & {
  steps: Step[];
  currentStep: number;
  completedIcon?: React.ReactNode;
  pendingIcon?: React.ReactNode;
};

export const Stepper: FC<StepperProps> = ({ steps, currentStep, completedIcon, pendingIcon }) => {
  const { colors } = useMantineTheme();

  return (
    <MantineStepper
      active={currentStep}
      orientation="vertical"
      iconSize={32}
      radius="md"
      allowNextStepsSelect={false}
      color={colors.etherlink[3]}
      completedIcon={completedIcon || <IconCircleCheck style={{ width: 18, height: 18 }} />}
      icon={pendingIcon || <IconClockHour2 style={{ width: 18, height: 18 }} />}
    >
      {steps.map((step, index) => (
        <MantineStepper.Step
          key={index}
          label={
            <Text fs="italic" fw={500}>
              {step.label}
            </Text>
          }
          description={step.description}
          icon={step.icon}
        />
      ))}
    </MantineStepper>
  );
};
