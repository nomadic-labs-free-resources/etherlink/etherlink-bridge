import React, { FC } from 'react';
import { ButtonProps } from '@mantine/core';
import { useAccount } from '@nlfr/use-tezos';
import { shortAddress } from '@/utils/utils';
import { useDisclosure } from '@mantine/hooks';
import { TezosIcon } from '@/icons/TezosIcon/TezosIcon';
import { IconChevronRight } from '@tabler/icons-react';
import { ProfileModal } from '../ProfileModal/ProfileModal';
import { ButtonGradient } from '../ButtonGradient/ButtonGradient';

type WalletType = 'eth' | 'tezos';

interface WalletButtonProps extends ButtonProps {
  walletType?: WalletType;
  onConnect?: () => void;
  onDisconnect?: () => void;
  onSwitchAccount?: () => void;
  shorterAddress?: boolean;
}

export const ButtonWallet: FC<WalletButtonProps> = ({
  walletType,
  onConnect,
  onDisconnect,
  onSwitchAccount,
  shorterAddress,
  ...buttonProps
}) => {
  const { address, connect } = useAccount();
  const [opened, { open, close }] = useDisclosure(false);
  // const { colors } = useMantineTheme();

  const handleConnectClick = () => {
    if (onConnect) {
      onConnect();
    } else {
      connect(); // Default connect method, adjust as needed for Ethereum
    }
  };

  return address?.length ? (
    <>
      <ButtonGradient
        onClick={open}
        fullWidth={buttonProps.fullWidth}
        leftSection={<TezosIcon />}
        rightSection={<IconChevronRight size="1.2rem" />}
      >
        {shorterAddress ? shortAddress(String(address)) : address}
      </ButtonGradient>
      <ProfileModal isOpen={opened} closeIt={close} address={address} walletType="tezos" />
    </>
  ) : (
    <ButtonGradient
      onClick={handleConnectClick}
      fullWidth={buttonProps.fullWidth}
      leftSection={<TezosIcon />}
    >
      Connect Tezos Wallet
    </ButtonGradient>
  );
};
