import React, { FC } from 'react';
import { Avatar, ButtonProps } from '@mantine/core';
import { IconChevronRight } from '@tabler/icons-react';
import { useWeb3Modal } from '@web3modal/wagmi/react';
import { useAccount } from 'wagmi';
import { shortEvmAddress } from '@/utils/utils';
import { useDisclosure } from '@mantine/hooks';
import { ProfileModal } from '../ProfileModal/ProfileModal';
import { ButtonGradient } from '../ButtonGradient/ButtonGradient';

type WalletType = 'eth' | 'tezos';

interface WalletButtonProps extends ButtonProps {
  walletType?: WalletType;
  onConnect?: () => void;
  onDisconnect?: () => void;
  onSwitchAccount?: () => void;
  shorterAddress?: boolean;
}

export const ButtonWalletEvm: FC<WalletButtonProps> = ({
  walletType,
  onConnect,
  onDisconnect,
  onSwitchAccount,
  shorterAddress,
  ...buttonProps
}) => {
  const { open: openWagmi } = useWeb3Modal();
  const { address } = useAccount();
  const [opened, { open, close }] = useDisclosure(false);

  const handleConnectClick = () => {
    if (onConnect) {
      onConnect();
    } else {
      openWagmi();
    }
  };

  return address?.length ? (
    <>
      <ButtonGradient
        onClick={open}
        fullWidth={buttonProps.fullWidth}
        leftSection={<Avatar size="sm" src="/assets/etherlink.svg" />}
        rightSection={<IconChevronRight size="1.2rem" />}
      >
        {shorterAddress ? shortEvmAddress(address) : address}
      </ButtonGradient>
      <ProfileModal isOpen={opened} closeIt={close} address={address} walletType="etherlink" />
    </>
  ) : (
    <ButtonGradient
      onClick={handleConnectClick}
      fullWidth={buttonProps.fullWidth}
      leftSection={<Avatar size="sm" src="/assets/etherlink.svg" />}
    >
      Connect Etherlink Wallet
    </ButtonGradient>
  );
};
