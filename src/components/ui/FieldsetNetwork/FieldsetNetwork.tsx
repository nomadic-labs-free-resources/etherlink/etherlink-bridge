import React, { FC, PropsWithChildren } from 'react';
import Image from 'next/image';
import { Group, Text, Fieldset } from '@mantine/core';

import { TezosIcon } from '@/icons/TezosIcon/TezosIcon';

type Props = {
  network: 'etherlink' | 'tezos';
};

export type FieldsetNetworkProps = PropsWithChildren<Props>;
export const FieldsetNetwork: FC<FieldsetNetworkProps> = ({ children, network }) => {
  const renderLegend = () =>
    network === 'tezos' ? (
      <Group gap="xs" align="center" px="md">
        <TezosIcon />
        <Text size="lg" fw="bold" fs="italic">
          Tezos
        </Text>
      </Group>
    ) : (
      <Group gap="xs" align="center" px="md">
        <Image src="/assets/etherlink.svg" alt="Etherlink Icon" width={20} height={20} />
        <Text size="lg" fw="bold" fs="italic">
          Etherlink
        </Text>
      </Group>
    );

  return (
    <Fieldset legend={renderLegend()} p="sm">
      {children}
    </Fieldset>
  );
};
