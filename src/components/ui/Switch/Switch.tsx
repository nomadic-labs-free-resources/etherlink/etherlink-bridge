import { Switch } from '@mantine/core';
import { FC } from 'react';

type Props = {
  checked: boolean;
  label: string;
  setChecked: (checked: boolean) => void;
};
export const SwitchM: FC<Props> = ({ checked, label, setChecked }) => (
  <Switch
    checked={checked}
    onChange={() => {
      setChecked(!checked);
    }}
    label={label}
  />
);
