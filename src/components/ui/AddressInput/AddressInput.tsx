'use client';

import { TextInput as MantineTextInput } from '@mantine/core';

import { FC } from 'react';
import { UseFormReturnType } from '@mantine/form';

type Props = {
  form: UseFormReturnType<any>;
  placeholder: string;
  inputPropsKey: string;
};

export const AddressInput: FC<Props> = (props) => (
  <MantineTextInput
    placeholder={props.placeholder}
    error={props.form.errors[props.inputPropsKey]}
    errorProps={{ fw: 'normal', fz: 'xs' }}
    size="lg"
    rightSectionWidth={150}
    fw="bolder"
    {...props.form.getInputProps(props.inputPropsKey)}
  />
);
