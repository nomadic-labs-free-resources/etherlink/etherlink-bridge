import { FC, useEffect, useState } from 'react';
import { Box, SegmentedControl } from '@mantine/core';

type Props = {
  activeTab: string;
  tabs: {
    title: string;
    component: React.ReactNode;
    disabled?: boolean;
  }[];
  onTabChange: (title: string) => void;
};

export type TabsSegmentedProps = Props;
export const TabsSegmented: FC<TabsSegmentedProps> = ({ activeTab, tabs, onTabChange }) => {
  const [selectedTab, setSelectedTab] = useState(activeTab || tabs[0].title);

  useEffect(() => {
    setSelectedTab(activeTab);
  }, [activeTab]);

  const handleTabChange = (value: string) => {
    setSelectedTab(value);
    onTabChange(value);
  };

  // Generate data array for SegmentedControl based on segments props
  const controlData = tabs.map(({ title, disabled }) => ({ label: title, value: title, disabled }));
  // Find the currently selected segment to render its component
  const selectedComponent = tabs.find((segment) => segment.title === selectedTab)?.component;

  return (
    <Box>
      <SegmentedControl
        value={activeTab}
        onChange={handleTabChange}
        data={controlData}
        transitionDuration={400}
        transitionTimingFunction="linear"
        size="md"
        fullWidth
      />
      <Box mt="md">{selectedComponent}</Box>
    </Box>
  );
};
