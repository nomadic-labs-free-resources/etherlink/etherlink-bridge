import React, { FC } from 'react';
import { Badge as MantineBadge, BadgeProps as MantineBadgeProps } from '@mantine/core';

import { TransferStatus } from '@/lib/use-outbox-indexer';

const getBadgeProps = (status: TransferStatus) => {
  let color;
  let text;

  switch (status) {
    case TransferStatus.PENDING:
      color = 'yellow';
      text = 'Pending';
      break;
    case TransferStatus.READY:
      color = 'blue';
      text = 'Ready';
      break;
    case TransferStatus.CONFIRMED:
      color = 'green';
      text = 'Confirmed';
      break;
    case TransferStatus.FAILED:
      color = 'red';
      text = 'Failed';
      break;
    default:
      color = 'gray';
      text = 'Unknown';
  }

  return { color, text };
};

type Props = {
  status?: TransferStatus;
};

export type BadgeProps = Props & MantineBadgeProps;
export const Badge: FC<BadgeProps> = ({ status, ...rest }) => {
  const { color, text } = status
    ? getBadgeProps(status)
    : { color: rest.color, text: rest.children };

  return (
    <MantineBadge color={color} fullWidth variant="light">
      {text}
    </MantineBadge>
  );
};
