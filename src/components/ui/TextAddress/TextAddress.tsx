'use client';

import React, { FC } from 'react';
import { Group, Text, CopyButton, ActionIcon, useMantineTheme, Box } from '@mantine/core';
import { IconCopy, IconCheck, IconExternalLink } from '@tabler/icons-react';

import { env } from '@/env.mjs';
import { Paper } from '../Paper/Paper';

type Props = {
  address: string;
  network?: 'tezos' | 'etherlink';
  tx?: boolean;
};

export type TextAddressProps = Props;
export const TextAddress: FC<TextAddressProps> = ({ address, network = 'tezos', tx = false }) => {
  const { colors } = useMantineTheme();

  const networkUrl =
    network === 'tezos'
      ? env.NEXT_PUBLIC_TZ_EXPLORER_URL
      : `${env.NEXT_PUBLIC_ETHERLINK_EXPLORER_URL}/${tx ? 'tx' : 'address'}`;

  return (
    <Paper w="100%">
      <Group justify="space-between" gap={0}>
        <Box>
          <Text component="pre" fz="sm" ff="mono" c={colors.green[4]} truncate="end">
            {address}
          </Text>
        </Box>
        <Box>
          <CopyButton value={address} timeout={2000}>
            {({ copied, copy }) => (
              <ActionIcon
                variant="transparent"
                color={copied ? 'teal' : 'gray'}
                onClick={copy}
                size="xs"
                style={{ marginLeft: 8 }}
              >
                {copied ? <IconCheck size={14} /> : <IconCopy size={14} />}
              </ActionIcon>
            )}
          </CopyButton>
          <ActionIcon
            variant="transparent"
            color="gray"
            size="xs"
            style={{ marginLeft: 8 }}
            component="a"
            target="_blank"
            href={`${networkUrl}/${address}`}
          >
            <IconExternalLink size={14} />
          </ActionIcon>
        </Box>
      </Group>
    </Paper>
  );
};
