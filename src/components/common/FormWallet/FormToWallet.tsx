import React, { FC } from 'react';
import { Grid, Center, Card, ButtonProps } from '@mantine/core';
import { ButtonWalletEvm } from '../../ui';

type WalletType = 'eth' | 'tezos';

interface FormFieldProps {
  label: string;
  buttonContent: React.ReactNode;
  buttonProps?: ButtonProps;
  walletType?: WalletType; // Optional prop to specify wallet type
  onConnect?: () => void; // Simplified connect handler, assuming event parameter is not needed
}

export const FormToWallet: FC<FormFieldProps> = ({
  label,
  buttonContent,
  buttonProps,
  walletType,
  // onConnect,
}) => (
  <Grid align="center" justify="center">
    <Grid.Col span="content">
      <Center style={{ fontWeight: 'bold' }}>{label}</Center>
    </Grid.Col>
    <Grid.Col span="auto">
      {walletType ? (
        <ButtonWalletEvm
          walletType={walletType}
          // onConnect={onConnect}
          {...buttonProps}
        >
          {buttonContent}
        </ButtonWalletEvm>
      ) : (
        <Card>{buttonContent}</Card>
      )}
    </Grid.Col>
  </Grid>
);
