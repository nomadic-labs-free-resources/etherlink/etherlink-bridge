import React, { useState } from 'react';
import { Button, Group, Divider, Center, Checkbox, Flex, Anchor } from '@mantine/core';
import { IconArrowDown, IconExternalLink } from '@tabler/icons-react';

import { env } from '@/env.mjs';
import { FieldsetNetwork, Modal } from '@/components/ui';
import {
  TransferDetailsBalanceUpdate,
  TransferDetailsBalanceUpdateProps,
} from '@/features/TransfersHistory/TransferDetails/TransferDetailsBalanceUpdate';
import { WithdrawAlert } from '../WithdrawAlert/WithdrawAlert';

type TransferNetworkUpdate = {
  network: TransferDetailsBalanceUpdateProps['network'];
  address: string;
  update: TransferDetailsBalanceUpdateProps['update'];
};

export enum TransferConfirmAction {
  Deposit = 'Deposit',
  Withdraw = 'Withdraw',
}

interface ConfirmModalProps {
  isOpen: boolean;
  onClose: () => void;
  onConfirm: () => Promise<void>;
  amount: number | string;
  ethAddress: string;
  tzAddress?: string;
  actionType: TransferConfirmAction;
}

export const ConfirmModal: React.FC<ConfirmModalProps> = ({
  isOpen,
  onClose,
  onConfirm,
  amount,
  ethAddress,
  tzAddress,
  actionType,
}) => {
  const hasTerms = env.NEXT_PUBLIC_DISCLAIMER && env.NEXT_PUBLIC_DISCLAIMER.length;
  const [termsChecked, setTermsChecked] = useState(false);
  const [isButtonLoading, setIsButtonLoading] = useState(false);

  const isDeposit = actionType === TransferConfirmAction.Deposit;
  const from: TransferNetworkUpdate = {
    network: isDeposit ? 'tezos' : 'etherlink',
    address: isDeposit ? String(tzAddress) : String(ethAddress),
    update: 'decrease',
  };
  const to: TransferNetworkUpdate = {
    network: isDeposit ? 'etherlink' : 'tezos',
    address: isDeposit ? String(ethAddress) : String(tzAddress || 'Undefined'),
    update: 'increase',
  };

  const handleConfirm = async () => {
    setIsButtonLoading(true);

    try {
      await onConfirm();
      onClose();
    } finally {
      setIsButtonLoading(false);
    }
  };

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      title={{
        title: `Confirm ${actionType}`,
        date: `Please review your ${actionType.toLowerCase()} details`,
        status: null,
      }}
    >
      {actionType === TransferConfirmAction.Withdraw && <WithdrawAlert />}
      <FieldsetNetwork network={from.network}>
        <TransferDetailsBalanceUpdate
          address={from.address}
          amount={Number(amount)}
          network={from.network}
          update={from.update}
        />
      </FieldsetNetwork>

      <Center my="md">
        <IconArrowDown size="1.2rem" />
      </Center>

      <FieldsetNetwork network={to.network}>
        <TransferDetailsBalanceUpdate
          address={to.address}
          amount={Number(amount)}
          network={to.network}
          update={to.update}
        />
      </FieldsetNetwork>

      {hasTerms && (
        <>
          <Divider my="sm" />
          <Flex direction="row">
            <Checkbox
              checked={termsChecked}
              onChange={(event) => setTermsChecked(event.currentTarget.checked)}
              label={
                <>
                  I agree to the{' '}
                  <Anchor href="/terms" target="_blank" inherit>
                    terms and conditions
                    <IconExternalLink size="0.8rem" />
                  </Anchor>
                </>
              }
              my="md"
              fw="bold"
              fs="italic"
            />
          </Flex>
        </>
      )}

      <Divider my="sm" />

      <Group p="right" mt="md" grow>
        <Button variant="default" onClick={onClose}>
          Cancel
        </Button>
        <Button
          color="green"
          onClick={handleConfirm}
          disabled={hasTerms ? !termsChecked : false}
          loading={isButtonLoading}
        >
          Confirm {actionType}
        </Button>
      </Group>
    </Modal>
  );
};
