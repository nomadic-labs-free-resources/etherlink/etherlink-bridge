import { FC } from 'react';
import { Text, Group } from '@mantine/core';
import { IconWallet } from '@tabler/icons-react';
import { useGetEtherlinkBalance } from '@/hooks';
import { BalanceProps } from '../TezosWalletBalance/TezosWalletBalance';

export const EtherlinkWalletBalance: FC<BalanceProps> = ({ size }) => {
  const { address, balance } = useGetEtherlinkBalance();

  if (!address) {
    return null;
  }

  return (
    <Group justify="flex-start" align="center" gap="0.1rem" ml="xs">
      <IconWallet size={size || '0.8rem'} stroke={1.5} color="gray" />
      <Text fz={size ? 'lg' : 'xs'} c={balance.error ? 'red' : 'dimmed'}>
        {balance.error
          ? 'An error occured'
          : balance.isLoading
            ? 'loading balance...'
            : `${(Number(balance.data?.value) / 1e18).toFixed(5)} XTZ`}
      </Text>
    </Group>
  );
};
