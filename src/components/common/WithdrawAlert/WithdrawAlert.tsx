import { FC } from 'react';
import { Alert, Anchor } from '@mantine/core';
import { IconExternalLink, IconClockHour2 } from '@tabler/icons-react';

export const WithdrawAlert: FC = () => (
  <Alert variant="default" mb="xl" icon={<IconClockHour2 />} title="Bridging Time">
    Tokens that you bridge from Etherlink to Tezos are available for use on Tezos in two weeks.
    <br />
    Please read the{' '}
    <Anchor href="https://docs.etherlink.com/get-started/bridging" target="_blank" inherit>
      documentation <IconExternalLink size="0.8rem" />
    </Anchor>{' '}
    to learn more.
  </Alert>
);
