import { FC, useState } from 'react';
import { Alert, Anchor, Box } from '@mantine/core';
import { IconExternalLink, IconInfoCircle } from '@tabler/icons-react';

export const BetaDisclaimer: FC = () => {
  const [isClosed, setIsClosed] = useState(false);

  const icon = <IconInfoCircle />;

  if (isClosed) {
    return <Box p="md" />;
  }

  return (
    <Alert
      variant="light"
      withCloseButton
      onClose={() => setIsClosed(true)}
      icon={icon}
      title="“Etherlink“ is BETA software"
      my="md"
    >
      This bridge supports the deposit and withdrawal of XTZ on Etherlink. See{' '}
      <Anchor href="/terms" target="_blank" inherit>
        terms and conditions.
        <IconExternalLink size="0.8rem" />
      </Anchor>
    </Alert>
  );
};
