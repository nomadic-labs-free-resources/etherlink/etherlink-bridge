import { FC } from 'react';
import { Text, Anchor, Alert } from '@mantine/core';
import { IconInfoCircle, IconExternalLink } from '@tabler/icons-react';
import { env } from '@/env.mjs';

export const DepositTestnetAlert: FC = () => (
  <>
    {env.NEXT_PUBLIC_TZ_NETWORK !== 'mainnet' && (
      <Alert variant="light" my="sm" py="lg" icon={<IconInfoCircle />} title="Important">
        <Text size="sm">
          This is a testnet. You can request free Ghostnet ꜩ from
          <Anchor href="https://faucet.ghostnet.teztnets.com/" target="_blank">
            {' '}
            the faucet. <IconExternalLink size="0.8rem" />
          </Anchor>
        </Text>
      </Alert>
    )}
  </>
);
