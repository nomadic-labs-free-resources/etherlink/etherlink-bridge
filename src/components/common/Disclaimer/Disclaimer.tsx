import { FC } from 'react';
import { Text, Alert } from '@mantine/core';
import { IconInfoCircle } from '@tabler/icons-react';

import { env } from '@/env.mjs';

export const Disclaimer: FC = () => (
  <>
    {env.NEXT_PUBLIC_DISCLAIMER && env.NEXT_PUBLIC_DISCLAIMER.length && (
      <Alert
        variant="transparent"
        my="sm"
        py="lg"
        icon={<IconInfoCircle />}
        title="Legal Disclaimer"
      >
        <Text size="sm">{env.NEXT_PUBLIC_DISCLAIMER}</Text>
      </Alert>
    )}
  </>
);
