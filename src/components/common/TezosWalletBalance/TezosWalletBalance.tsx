import { FC } from 'react';
import { Text, Group } from '@mantine/core';
import { IconWallet } from '@tabler/icons-react';

import { useGetTezosBalance } from '@/hooks';

export interface BalanceProps {
  size?: string;
}

export const TezosWalletBalance: FC<BalanceProps> = ({ size }) => {
  const { address, balance } = useGetTezosBalance();

  if (!address) {
    return null;
  }

  return (
    <Group justify="flex-start" align="center" gap="0.1rem" ml="xs">
      <IconWallet size={size || '0.8rem'} stroke={1.5} color="gray" />
      <Text fz={size ? 'lg' : 'xs'} c="dimmed">
        {balance !== null ? `${balance} XTZ` : 'loading balance...'}
      </Text>
    </Group>
  );
};
