import { FC } from 'react';
import { Menu, Button, rem, ActionIcon, Group, Box, useMantineColorScheme } from '@mantine/core';
import { IconSettings, IconArrowsLeftRight, IconSun, IconMoonStars } from '@tabler/icons-react';
import { env } from '@/env.mjs';

type Props = {
  menuTargetLabel?: boolean;
};

export type ConfigMenuProps = Props;
export const ConfigMenu: FC<ConfigMenuProps> = ({ menuTargetLabel }) => {
  const { colorScheme, toggleColorScheme } = useMantineColorScheme();

  return (
    <Menu shadow="md" width={210} closeOnItemClick={false}>
      <Menu.Target>
        {menuTargetLabel ? (
          <Button
            variant="default"
            leftSection={<IconSettings style={{ width: rem(14), height: rem(14) }} stroke={1.5} />}
            fullWidth
          >
            Settings
          </Button>
        ) : (
          <ActionIcon variant="default" size="lg">
            <IconSettings style={{ width: rem(18), height: rem(18) }} stroke={1.5} />
          </ActionIcon>
        )}
      </Menu.Target>

      <Menu.Dropdown>
        <Menu.Label>
          <Group justify="space-between">
            <Box> Network </Box>
            <Box fw="bold"> {env.NEXT_PUBLIC_TZ_NETWORK} </Box>
          </Group>
        </Menu.Label>

        <Menu.Item
          leftSection={<IconArrowsLeftRight style={{ width: rem(14), height: rem(14) }} />}
          component="a"
          href={env.NEXT_PUBLIC_APP_SWITCH_URL}
        >
          Switch to {env.NEXT_PUBLIC_TZ_NETWORK === 'mainnet' ? 'testnet' : 'mainnet'}
        </Menu.Item>

        <Menu.Divider />

        <Menu.Label>Theme</Menu.Label>

        <Menu.Item
          leftSection={
            colorScheme === 'dark' ? (
              <IconSun size="1.2rem" stroke={1.5} />
            ) : (
              <IconMoonStars size="1.2rem" stroke={1.5} />
            )
          }
          onClick={() => toggleColorScheme()}
        >
          Switch to {colorScheme === 'dark' ? 'light' : 'dark'} theme
        </Menu.Item>
      </Menu.Dropdown>
    </Menu>
  );
};
