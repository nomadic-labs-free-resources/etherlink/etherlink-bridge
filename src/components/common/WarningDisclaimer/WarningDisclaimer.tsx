import { FC, useState } from 'react';
import { Alert, Anchor, Box, Text } from '@mantine/core';
import { IconExternalLink, IconInfoCircle } from '@tabler/icons-react';

export const WarningDisclaimer: FC = () => {
  const [isClosed, setIsClosed] = useState(false);

  const icon = <IconInfoCircle />;

  if (isClosed) {
    return <Box p="md" />;
  }

  return (
    <Alert
      variant="light"
      withCloseButton
      onClose={() => setIsClosed(true)}
      icon={icon}
      title="ParisC Announcement: Withdrawal Claim Delay"
      my="md"
      color="orange"
    >
      We have uncovered an issue delaying the claim of withdrawals performed on Etherlink between
      May 21 and June 11. <br />
      <Text inherit component="span" fw="bold">
        {'Your funds are not in danger '}
      </Text>{' '}
      and will be available to you on June 26 at latest. Withdrawals performed after June 11 should
      not be affected. <br />
      Please see the{' '}
      <Anchor
        href="https://research-development.nomadic-labs.com/parisC-announcement.html"
        target="_blank"
        inherit
      >
        ParisC announcement <IconExternalLink size="0.8rem" />
      </Anchor>{' '}
      to learn more.
    </Alert>
  );
};
