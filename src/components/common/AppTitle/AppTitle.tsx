'use client';

import { FC } from 'react';
import { Text, Title, useMantineTheme } from '@mantine/core';

import classes from './AppTitle.module.css';

export const AppTitle: FC = () => {
  const { primaryColor } = useMantineTheme();

  return (
    <Title size="h2" className={classes.etherlinkTitle}>
      <Text inherit component="span" className={classes.etherlinkSpan}>
        Etherlink
      </Text>
      &nbsp;
      <Text inherit component="span" c={primaryColor}>
        Bridge
      </Text>
    </Title>
  );
};
