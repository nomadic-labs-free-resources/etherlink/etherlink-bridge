# Etherlink Bridge

Etherlink Bridge is an interface designed for bridging XTZ from the Tezos blockchain to Etherlink.

## Getting Started

### Prerequisites

- Node.js: Version 18.15.0 or higher is recommended.
- Bun: Version 1.0.25 or higher. This project utilizes Bun for its speed and efficiency. If you prefer npm or Yarn, please adjust the commands accordingly and ensure compatibility.

### Environment Variables

Create a `.env.local` file in the root directory and add your environment-specific variables or modify the `.env`.

### Installation

1. __Clone the repository__:
```bash
git clone https://gitlab.com/nomadic-labs-free-resources/etherlink/etherlink-bridge.git
```

2. __Navigate to the project directory__:
```bash
cd etherlink-bridge
```

3. __Install dependencies__:
```bash
bun install
```

This command installs all necessary dependencies as defined in the project's `package.json` file.

4. __Environment Setup__:

- Create a .env.local file in the root directory for your local development environment.
- Alternatively, adjust the existing `.env` file with necessary environment-specific variables for production or other stages.

5. __Development server__:
```bash
bun dev
```

- This command starts the local development server.
- Visit http://localhost:3000 in your browser to view the application.

6. __Production Build and Start__:

```bash
bun build
bun start
```

- Use these commands to compile the application for production and start the production server.

## Docker Support

### Build and Run Container

```bash
docker build -t etherlink-bridge .
docker run -p 3000:3000 etherlink-bridge
```

These commands build a Docker image for the Etherlink Bridge application and run it as a container, making it accessible at http://localhost:3000.

### Compose

```bash
docker compose up -d
```

Leverage Docker Compose for simplified orchestration. This command starts the application in detached mode, allowing it to run in the background.

## Deployment

The project can be deployed using Vercel, Netlify, or any other hosting service that supports Next.js. Refer to the documentation of your chosen service for deployment instructions.

## License

Etherlink Bridge is licensed under the MIT License. See [LICENSE](./LICENSE) for details.
